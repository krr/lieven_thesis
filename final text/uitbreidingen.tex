\chapter{Uitbreidingen op het vertaalsysteem}
\label{hoofdstuk:uitbreidingen}
In dit hoofdstuk worden een aantal uitbreiding van het vertaalsysteem bestudeerd die niet ge\"implementeerd werden in de huidige versie. Er wordt nagegaan hoe deze ge\"integreerd kunnen worden en welke voordelen ze bieden. In hoofdstuk \ref{hoofdstuk:besluit} worden er andere, nog niet bestudeerde uitbreidingen en aanpassingen voorgesteld als verder onderzoek.

\section{Vertalen van LTC theorie\"en}
\label{sec:extension_LTC}
Binnen IDP kan Linea\"ire Tijdscalculus of LTC \cite{bogaertssimulating}gebruikt worden voor het beschrijven van dynamische systemen. Een LTC theori\"e wordt beschreven in termen van \textit{action} en \textit{fluent} predicaten/functies en een \textit{Time type}. De fluents beschrijven de toestand van het dynamisch systeem op elk (geldig) tijdstip, aangegeven als een nummer van het type Time. De actions geven de voorwaarden aan waaronder de toestand van het systeem wijzigt in verschillende tijdstippen. Met de functie 'Next(Time)' kan het volgende tijdstip opgevraagd worden. Aan de hand van een FO(.) definitieblok wordt voor elke fluent het volgende beschreven: de \textit{init\"eele toestand}, de \textit{inertia axioms} of onder welke acties ze haar toestand uit het vorige tijdstip op het volgende tijdstip behoudt en de \textit{state change axioms} die aangeeft hoe en onder welke acties een fluent verandert op het volgend tijdstip.

Er wordt typisch een afzonderlijk \textit{LTC vocabularium }gedefini\"eerd. Dit vocabularium gebruikt dezelfde syntax als een gewoon vocabularium en kan als dusdanig ge\"implementeerd worden in het vertaalsysteem. Het verschil is dat een LTC vocabularium toelaat om een andere naam voor het tijdstype 'Time' te geven, een andere starttijd en een andere 'successorfunctie' voor 'Time'. Voor een formele beschrijving wordt de lezer verwezen naar de LTC paper. Een eenvoudige LTC specificatie met ge\"annoteerd vocabularium ziet er als volgt uit:

\begin{lstlisting}[caption=Een simpele LTC specificatie met ge\"annoteerd vocabularium,breaklines=true,mathescape=true]
LTCvocabulary V (MyTime, MyStart , MyNext ) {
	type MyTime isa int #TYPE THING description"time #"
	type State isa bool. #TYPE THING description"state #"
	MyStart :MyTime #FUNCTION description"the start time #R" ="the start time"
	MyNext (MyTime ) : MyTime #FUNCTION"the time after #1 is #R" ="the time after #1"
	
	some_action(Mytime) #PREDICATE description"at time #1, some action occurs" $\neg$description"at time #1, some action doesn't occur"
	some_fluent(Mytime,State) #PREDICATE description"at time #1, some fluent is in #2" $\neg$description"at time #1, some fluent is not in #2"
}

theory T:V {
	{
	//initial state
	$some_fluent(MyStart,false) \leftarrow true.$
	//inertia
	$some\_fluent(MyNext(t),false) \leftarrow \neg some\_action(t) \wedge some\_fluent(t,false).$
	$some\_fluent(MyNext(t),true) \leftarrow \neg some\_action(t) \wedge some\_fluent(t,true).$
	//state_change
	$some\_fluent(MyNext(t),true) \leftarrow some\_action(t) \wedge some\_fluent(MyNext(t),false).$
	$some\_fluent(MyNext(t),false) \leftarrow some\_action(t) \wedge some\_fluent(MyNext(t),true).$
	}
	
}
\end{lstlisting}

Zonder veel aanpassingen aan het vertaalsysteem kan de bovenstaande definitieregel
$$some\_fluent(MyNext(t),true) \leftarrow ~some\_action(t) \wedge some\_fluent(t,true).$$
vertaald worden als
\begin{align*}
''\text{At time the time after a time, some fluent is in state true when at that time,}\\
\text{some action doesn't occur and at that time, some fluent is in state true}''
\end{align*}
Door het type $Time$ een speciale behandeling te geven, kan de vertaling natuurlijker gemaakt worden. De bovenstaande definitieregel verwijst naar twee verschillende tijdstippen. Op basis van die informatie wordt ze vertaald als
\begin{align*}
''\text{At the next time, some fluent is in state true when at the current time,}\\
\text{some action doesn't occur and at the current time, some fluent is in state true}''
\end{align*}

In \ref{sec:hanoi} werd reeds een theorie vertaald die een dynamisch systeem beschijft: de verschillende opeenvolgende toestanden van de Hano\"i puzzel. Daar werd het tijdsargument letterlijk vertaald. Merk op dat deze niet de vorm van een LTC specificatie heeft. Beschouw de eerste  definitie uit de Hano\"i puzzel. De eerste definitieregel heeft volgende vertaling:
$$''\text{At time 0 every disc is in the left position.}''$$
Het vertaalsysteem kan worden uitgebreid om 'tijdstippen' te vertalen met woorden:
$$''\text{Initially, every disc is in the left position.}''$$
De vertaling van de volgende definitieregel
\begin{align*}
''\text{At a Time plus time 1 the disc moved at time that Time is in the position}\\
\text{moved to at that Time when that Time is a point in time and a disc is a disc.}''
\end{align*}
bevat 2 tijdstippen waarnaar verwezen wordt. Een verbeterde vertaling is
\begin{align*}
''\text{At a next time, the disc moved at the current time is in the position moved at the}\\
\text{current time (when the current time is a point in time and a disc is a disc).}''
\end{align*}
Het is mogelijk om het vertaalsysteem de structuur van een LTC definities te laten analyseren. In principe is het mogelijk definitieregels met hetzelfde hoofd te combineren. Beschouw de 'inertia' en 'state change' definitieregels hierboven met $some_fluent(MyNext(t),true)$ als hoofd. Een 'samengestelde' vertaling is dan
\begin{align*}
''\text{At the next time, some fluent is true if that fluent is true at the current time}\\
\text{and some action doesn't occur at the current time, \textbf{or} that fluent is false at}\\
\text{the current time and that action occurs at the current time}''
\end{align*}

\section{Logische transformaties en standaardvorm}
\label{sec:extension_transformationdefaultform}
Het huidige vertaalsysteem genereert mogelijks verschillende vertalingen voor twee logisch equivalente zinnen over eenzelfde vocabularium. Een voorbeeld hiervan is het toepassen van de regels van De Morgan. Zo wordt de FO(.) zin $\neg(IsCool(John)\wedge IsSmart(John)).$ vertaald als ''It is not true that John is cool and John is smart.'', terwijl $\neg IsCool(John) \vee IsSmart(John)$ als vertaling ''John is not cool or John is not Smart'' heeft. De $\neg$ wordt dus niet doorgeduwd omdat bij complexere zinnen het voor een 'luie' lezer lastig wordt om te zien hoe alle en/of wisselen. De ''It is not true...'' wordt verondersteld te gelden over de rest van zin, maar dit kan ambigu\"iteit veroorzaken.

Deze problemen kunnen vermeden worden door de FO(.) zinnen in een theorie op voorhand in een standaardvorm te brengen. Dit kan de duidelijkheid en kwaliteit van de vertaling verbeteren. Bovendien moeten er dan enkel vertaalregels zijn voor de situaties die in de standaardvorm kunnen voorkomen, wat het totaal aantal regels reduceert. Om een standaardvorm voor FO(.) zinnen vast te leggen, wordt rekening gehouden worden met de volgende eigenschappen:

\begin{itemize}
	\item Alle kwantoren kunnen vooraan de zin geplaatst worden. $\neg\forall$ en $\not\exists$ kunnen worden getransformeerd tot $\exists\colon\neg...$ en $\forall\colon\neg...$, maar er moet rekening gehouden worden met hoe sterk de vertaling hierdoor transformeert. Wanneer alle kwantoren bij elkaar staan is het voor het vertaalsysteem heel eenvoudig om hun 'nesting' na te gaan: $\forall x\colon \exists y$, $\exists=1\colon \forall y$, etc... Zo wordt het bv. mogelijk om bij $$\exists x\colon \forall h\colon IsOwnedBy(h,x)$$ te vertalen met 'the same x' in plaats van 'some x': $$''\text{Every house is owned by \textit{the same} person.}''$$
	\item De meest natuurlijke vertalingen zijn die waarbij de $\neg$ bij de predicaten staat of als $\not=$ in een gelijkheid. De negaties worden best zo ver mogelijk 'doorgeduwd' in de FO(.) zin.
	\item Verkies het gebruik van geneste functies boven extra predicaten bij het ontwerpen van een FO(.) specificatie: de vertaling wordt hierdoor korter en meer gestructureerd. Bv. de zin $$FatherOf(FatherOf(John))=Steven$$ wordt vertaald als $$''\text{The father of the father of John is Steven.}''$$ wat te verkiezen is boven $$ \exists x\colon FatherOf(John,x) \wedge Father(Steven,x).$$ met als vertaling $$''\text{The father of John is some person and Steven is the father of that person.}''$$
	\item Wanneer een concept complex is om uit te leggen in termen van de bestaande vocabulariumsymbolen, kan er best een nieuw symbool met een eigen, eenvoudigere interpretatie worden ge\"introduceert. Bij de Einstein puzzel in \ref{sec:einstein} werd als voorbeeld reeds de 'buren' relatie aangehaald, welke dan kan beschreven worden door een definitieregel
	\begin{align*}
	Neighbor(p1,p2) \leftarrow \exists h1,h2\colon OwnsHouse(p1,h2)\\
	\wedge OwnsHouse(p2,h2) \wedge abs(Order(h2)-Order(h1))==1.
	\end{align*}
\end{itemize}

Soms bevat een FO(.) zin een bepaald 'logisch patroon' dat door een (automatische) transformatie eenvoudiger vertaald wordt. Deze transformatie kan zowel voorafgaand aan de vertaling, door het vertaalsysteem zelf of in een post-processing stap gebeuren. Beschouw bv. een reeks van 'en' operatoren in een zin $$FriendsWith(John,Steven) \wedge FriendsWith(John,Bob) \wedge FriendsWith(John,Dave).$$ Normaal gezien is de vertaling $$''\text{John is friends with Steven and John is friends with Bob and...}''$$ Indien een soort 'transformatieregel' $FriendsWith(A,B)\wedge FriendsWith(A,C) \mapsto FriendsWith(A,B,C)$ gekend is, kan de vertaling gecombineerd worden: $$''\text{John is friends with Steven, Bob and Dave.}''$$ Een gelijkaardig principe kan gebruikt worden om de vertaling van FO(.) structuren korter te maken.

\section{Besluit van dit hoofdstuk}
In dit hoofdstuk werden enkele uitbreidingen bestudeerd. Er werd nagegaan welke aanpassing er moeten gebeuren aan het vertaalsysteem om LTC theorie\"en te kunnen vertalen: het blijkt voldoende om het tijdstype op een speciale manier te behandelen en de vertaling van definities te optimaliseren. Daarnaast werd nagegaan wat de voordelen zijn van het gebruik van een logische standaardvorm, zodat equivalente FO(.) zinnen eenzelfde vertaling krijgen. Er werd bestudeerd hoe logische transformaties van (de NL interpretaties van) formules of predicaten tot betere of kortere vertalingen kunnen leiden.

%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "masterproef"
%%% End: 
