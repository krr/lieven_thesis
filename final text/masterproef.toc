\select@language {dutch}
\select@language {dutch}
\select@language {dutch}
\select@language {dutch}
\select@language {dutch}
\select@language {dutch}
\select@language {dutch}
\contentsline {chapter}{Voorwoord}{i}{chapter*.1}
\contentsline {chapter}{Samenvatting}{iv}{chapter*.2}
\contentsline {chapter}{L"yst van figuren}{v}{section*.3}
\contentsline {chapter}{Lijst van afkortingen en symbolen}{vi}{chapter*.4}
\contentsline {chapter}{\chapternumberline {1}Inleiding}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}Introductie}{1}{section.1.1}
\contentsline {section}{\numberline {1.2}Probleemstelling}{4}{section.1.2}
\contentsline {section}{\numberline {1.3}Structuur van de thesis}{4}{section.1.3}
\contentsline {section}{\numberline {1.4}Bijkomende informatie}{4}{section.1.4}
\contentsline {chapter}{\chapternumberline {2}Literatuurstudie}{5}{chapter.2}
\contentsline {section}{\numberline {2.1}Achtergrondinformatie}{5}{section.2.1}
\contentsline {subsection}{\numberline {2.1.1}FO(.) logica}{5}{subsection.2.1.1}
\contentsline {subsection}{\numberline {2.1.2}Het IDP systeem}{5}{subsection.2.1.2}
\contentsline {subsection}{\numberline {2.1.3}Lineaire Tijd Calculus}{6}{subsection.2.1.3}
\contentsline {subsection}{\numberline {2.1.4}Natuurlijke Taal Generatie}{6}{subsection.2.1.4}
\contentsline {section}{\numberline {2.2}Overzicht van gerelateerd onderzoek}{7}{section.2.2}
\contentsline {chapter}{\chapternumberline {3}FO(.) syntax}{11}{chapter.3}
\contentsline {section}{\numberline {3.1}Een FO(.) vocabularium}{11}{section.3.1}
\contentsline {section}{\numberline {3.2}Een FO(.) formule}{11}{section.3.2}
\contentsline {section}{\numberline {3.3}Een FO(.) definitie}{12}{section.3.3}
\contentsline {section}{\numberline {3.4}Een FO(.) theorie}{12}{section.3.4}
\contentsline {section}{\numberline {3.5}Een FO(.) structuur}{12}{section.3.5}
\contentsline {chapter}{\chapternumberline {4}Overzicht van het vertaalsysteem}{15}{chapter.4}
\contentsline {section}{\numberline {4.1}Parsen van de FO(.) specificatie}{15}{section.4.1}
\contentsline {section}{\numberline {4.2}Annotaties in het vocabularium}{16}{section.4.2}
\contentsline {section}{\numberline {4.3}Typeinferentie}{19}{section.4.3}
\contentsline {section}{\numberline {4.4}Vertalen volgens AST, compositie en contextpropagatie}{20}{section.4.4}
\contentsline {section}{\numberline {4.5}Besluit van dit hoofdstuk}{24}{section.4.5}
\contentsline {chapter}{\chapternumberline {5}Vertaalregels en Compleetheid}{25}{chapter.5}
\contentsline {section}{\numberline {5.1}Formele beschrijving van vertaalregels}{25}{section.5.1}
\contentsline {section}{\numberline {5.2}Overzicht vertaalregels}{28}{section.5.2}
\contentsline {subsection}{\numberline {5.2.1}FO(.) formules in FO(.) theorie}{28}{subsection.5.2.1}
\contentsline {paragraph}{Een logische unaire operator}{28}{section*.9}
\contentsline {paragraph}{Een $\forall $ of $\exists $ kwantor}{30}{section*.10}
\contentsline {paragraph}{Een $\exists =I$, $\exists \geq X$ of $\exists \leq X$ kwantor}{31}{section*.11}
\contentsline {paragraph}{Een binaire logische operator}{32}{section*.12}
\contentsline {paragraph}{Een unaire aritmetische operator}{33}{section*.13}
\contentsline {paragraph}{Een $=$-of $\not =$-expressie}{34}{section*.14}
\contentsline {paragraph}{Een vergelijkingsoperator}{37}{section*.15}
\contentsline {paragraph}{Een binaire aritmetische operator}{37}{section*.16}
\contentsline {paragraph}{Een term}{38}{section*.17}
\contentsline {paragraph}{Een definitie blok van $\leftarrow $ regels}{43}{section*.18}
\contentsline {paragraph}{Een aggregaatfunctie}{44}{section*.19}
\contentsline {paragraph}{Een hele FO(.) theorie}{45}{section*.20}
\contentsline {subsection}{\numberline {5.2.2}FO(.) structuur}{45}{subsection.5.2.2}
\contentsline {paragraph}{Bereik van een type}{45}{section*.21}
\contentsline {paragraph}{Een beschrijving van domeinelementen}{46}{section*.22}
\contentsline {paragraph}{Een 'driewaardige' interpretatie}{47}{section*.23}
\contentsline {paragraph}{Een volledige FO(.) structuur}{47}{section*.24}
\contentsline {section}{\numberline {5.3}Compleetheidsargument}{48}{section.5.3}
\contentsline {section}{\numberline {5.4}Besluit van dit hoofdstuk}{48}{section.5.4}
\contentsline {chapter}{\chapternumberline {6}Resultaten}{49}{chapter.6}
\contentsline {section}{\numberline {6.1}Relaties in een familie}{49}{section.6.1}
\contentsline {section}{\numberline {6.2}De Einstein Puzzel}{52}{section.6.2}
\contentsline {section}{\numberline {6.3}De Torens van Hanoi}{56}{section.6.3}
\contentsline {section}{\numberline {6.4}Besluit van dit hoofdstuk}{60}{section.6.4}
\contentsline {chapter}{\chapternumberline {7}Uitbreidingen op het vertaalsysteem}{61}{chapter.7}
\contentsline {section}{\numberline {7.1}Vertalen van LTC theorie\"en}{61}{section.7.1}
\contentsline {section}{\numberline {7.2}Logische transformaties en standaardvorm}{63}{section.7.2}
\contentsline {section}{\numberline {7.3}Besluit van dit hoofdstuk}{65}{section.7.3}
\contentsline {chapter}{\chapternumberline {8}Besluit}{67}{chapter.8}
\contentsline {section}{\numberline {8.1}Reflectie en Conclusie}{67}{section.8.1}
\contentsline {section}{\numberline {8.2}Verder onderzoek}{69}{section.8.2}
\contentsline {appendix}{\chapternumberline {A}Implementatie van het vertaalsysteem}{73}{appendix.A}
\contentsline {appendix}{\chapternumberline {B}Gebruikte FO(.) specificaties}{75}{appendix.B}
\contentsline {section}{\numberline {B.1}Relaties in een familie}{75}{section.B.1}
\contentsline {section}{\numberline {B.2}De Einstein puzzel}{80}{section.B.2}
\contentsline {section}{\numberline {B.3}De torens van Hanoi}{83}{section.B.3}
\contentsline {section}{\numberline {B.4}Kortste pad tussen steden}{86}{section.B.4}
\contentsline {section}{\numberline {B.5}Logigram puzzel 6}{88}{section.B.5}
\contentsline {appendix}{\chapternumberline {C}Wetenschappelijke paper}{93}{appendix.C}
\contentsline {appendix}{\chapternumberline {D}Thesisposter}{101}{appendix.D}
\contentsline {chapter}{Bibliografie}{103}{section*.26}
\select@language {dutch}
\select@language {dutch}
\select@language {dutch}
\select@language {dutch}
\select@language {dutch}
\select@language {dutch}
\select@language {dutch}
\select@language {dutch}
\select@language {dutch}
\select@language {dutch}
