\chapter{Overzicht van het vertaalsysteem}
\label{hoofdstuk:overzicht}
Dit hoofdstuk geeft een overzicht van het vertaalsysteem dat ontworpen werd in de thesis. De werking van de verschillende onderdelen wordt besproken. Allereerste moet de te vertalen FO(.) specificatie \textit{geparsed} worden zodat de vertaler ermee kan werken. De ontbrekende of impliciet aanwezige semantische informatie moet expliciet gemaakt worden d.m.v. annotaties in het vocabularium. Vooraleer een vertaling kan worden opgesteld, is ook enige vorm van (term)typeinferentie noodzakelijk om de vertaalregels te doen werken. Daarna kan de gegeven FO(.) specificatie vertaald worden door de juiste vertaalregels toe te passen op elk niveau in de opgestelde \textit{abstracte syntaxboom}, door compositie van deelvertalingen en het propageren van contextinformatie. In hoofdstuk \ref{hoofdstuk:vertaalregels-compleetheid} worden deze vertaalregels formeel beschreven. Er werd tevens een implementatie gemaakt van het vertaalsysteem: voor meer informatie wordt de lezer doorverwezen naar appendix \ref{app:A}.

\section{Parsen van de FO(.) specificatie}
\label{sec:parsing}
Om een FO(.) specificatie te interpreteren en te vertalen, vertrekt het vertaalsysteem van een abstracte syntaxboom. Met een \textit{FO(.) specificatie} wordt het hele specificatie bedoeld dat aan het IDP systeem gegeven wordt en waar een logische inferentie kan op worden uitgevoerd. Met een \textit{abstracte syntaxboom} of \textit{AST} wordt de intern gebruikte boomvoorstelling van de geparste specificatie bedoeld. Uiteraard kan een FO(.) specificatie enkel geparset worden als de syntax voldoet aan een bepaalde grammatica. Het vertaalsysteem gebruikt de FO(.) syntax uit hoofdstuk \ref{hoofdstuk:fodot-syntax} uitgebreid met vocabularium annotaties.  In sectie \ref{sec:voc_annotations} wordt de syntax van de vocabularium annotaties beschreven.

Een syntaxboom is een populaire manier binnen NLG applicaties om het 'te vertalen' op een systematische wijze voor te stellen. In zo'n boomstructuur kan in elke node extra contextinformatie die noodzakelijk is voor de werking van het NLG systeem worden bijgehouden. De boomstructuur kan zelf \textit{getransformeerd} worden van een bron grammatica naar een doel grammatica, gevolgd door vertaling van de individuele nodes, of als invoer dienen bij het toepassen van een reeks (deterministische) vertaalregels die een vertaalde vorm genereren. In deze thesis wordt de syntaxboom op de tweede manier aangewend. Ze wordt opgesteld door een gegeven FO(.) specificatie te parsen volgens de FO(.) grammatica (zoals beschreven in hoofdstuk \ref{hoofdstuk:fodot-syntax} en \cite{de2014predicate}) uitgebreid met vocabulariumannotaties (zoals beschreven in de volgende sectie). Intern ziet deze AST er als volgt uit:

\begin{itemize}
	\item Elke node in de AST heeft een \textit{kind} of soort: dit geeft aan wat voor soort deelexpressie de node (en haar dochterboom) voorstellen.
	\item Elke node in de AST heeft een \textit{label} of type: dit geeft aan welke specifieke deelexpressie de node voorstelt binnen die kind. Er kunnen immers verschillende syntactische alternatieven zijn, met een andere vertaalregel voor elk alternatief.
	\item Elke node heeft een \textit{content} of inhoud: dit is in feite een lijst met de verschillende directe dochternodes die de node heeft. Deze lijst is leeg wanneer de node een blad is van de AST.
\end{itemize}

Sommige nodes kunnen nog extra eigenschappen hebben, afhankelijk van het niveau in de syntaxboom waar ze voorkomen:

\begin{itemize}
	\item Een \underline{vars} of variabelenlijst: sommige nodes corresponderen met een FO(.) expressie waarbij verschillende variabelen gekwantificeerd worden.
	\item Een \textit{annotation} of annotatiestructuur: sommige nodes corresponderen met de annotatie bij een definitie in een FO(.) vocabularium. Zo'n annotatie is op zich ook een boomstructuur en wordt verder beschreven in \ref{sec:voc_annotations}.
\end{itemize}
       
Merk op dat naast deze interne voorstelling van een node er ook andere, equivalente voorstellingen mogelijk zijn. Voor de formele beschrijving van de vertaalregels in hoofdstuk \ref{hoofdstuk:vertaalregels-compleetheid} wordt zo'n equivalente voorstelling beschreven en aangewend voor nodes die corresponderen met een FO(.)formule of een aritmetische expressie.

\section{Annotaties in het vocabularium}
\label{sec:voc_annotations}
Wanneer iemand een nieuwe FO(.) specificatie wil bouwen, moet deze een vocabularium van predicaat- en functienamen kiezen die gebruikt zullen worden. Zowel de naamgeving van deze symbolen als de argumenten die ze nemen (en de types ervan) zijn essentieel om hun betekenis duidelijk te maken. Wanneer iemand anders met kennis van FO(.) en IDP het ontworpen vocabularium leest, is het meestal snel duidelijk hoe de predicaten en functies ge\"interpreteerd dienen te worden, ook al geeft de ontwerper deze exacte interpretatie niet mee. Informatie omtrent de interpretatie van de symbolen kan komen uit de context waarbinnen de specificatie ontwikkeld werd, of vanuit (impliciete) conventies omtrent de naamgeving van predicaten en functies (bv. namen in het Engels, gebruik van bepaalde werkwoordsvormen of substantieven,etc...). Mensen beschikken over heel veel \textit{impliciete} achtergrondkennis, en aangezien FO(.) specificaties vaak een stuk van de r\"eele wereld trachten te beschrijven, kan deze kennis worden aangewend om ze eenvoudig (in natuurlijke taal) te interpreteren. Zo is het bv. duidelijk uit de context van een FO(.) specificatie over familiestructuren (zie tevens appendix \ref{app:B}) dat $ParentOf(x,y)$ ge\"interpreteerd dient te worden als ''$\text{x is a parent of some y}$''. Zelfs wanneer het vocabularium niet expliciet beschrijft dat het type van $x$ en $y$ $Person$ is, leidt een menselijke lezer vlot af dat $ParentOf(x,y)$ ge\"interpreteerd dient te worden als ''$\text{some person is a parent of another person}$'', aangezien mensen typisch 2 ouders hebben, een 'Persoon' typisch een mens is, en mensen normaal geen ouders van zichzelf zijn. Mensen zijn in staat te redeneren op basis van veronderstelde, onvolledige, impliciet aanwezige kennis van de beschreven wereld. Een machinaal vertaalsysteem kan zoiets niet, tenzij hiervoor een databank van impliciete wereldkennis aanwezig is en software om daarmee te werken. Daarnaast zal er een soort van leersysteem aanwezig moeten zijn om de databank en die software up-to-date te houden, iets wat op zich al niet vanzelfsprekend is. In de inleiding werd reeds gesteld dat deze thesis een vertaalaanpak zonder \textit{machine learning} methoden beoogt. Het automatisch interpreteren van predicaat- en functienamen wordt als mogelijke uitbreiding van het systeem ge\"analyseerd in hoofdstuk \ref{hoofdstuk:uitbreidingen}.

Het vertaalsysteem zou overweg moeten kunnen met onvolledige of impliciete informatie omtrent de interpretatie van vocabulariumsymbolen. Om deze reden werd er een \textit{annotatiesysteem} ontwikkeld waarmee een menselijke gebruiker de NL interpretatie van types, predicaten en functies, welke basisbouwstenen van FO(.) formules vormen, duidelijk kan maken aan het vertaalsysteem. In een FO(.) vocabularium $\psi$ kunnen zowel types, predicaten, functies en constanten een annotatie krijgen. Hieronder volgt een beschrijving van de verschillende mogelijke vocabularium annotaties met voorbeelden hoe ze gebruikt worden. De structuur van een annotatie bij een symboolbeschrijving in $\psi$ kan concreet beschreven worden door ''$\text{[iets]|[anders]}$'' of ''$\text{[iets|anders]}$'' te gebruiken om alternatieve delen aan te geven en ''$\text{(optioneel)}$'' om een optioneel deel aan te geven. \\

\noindent Een type in een FO(.) vocabularium $\psi$ wordt geannoteerd met:
\begin{itemize}
	\item een $\#TYPE$ tag gevolgd door $ENTITIY$ of $THING$ om aan te geven of instanties van het type dingen of entiteiten (personen, dieren, intelligente actoren) zijn. Dit is nodig om (recursieve) referenties goed te kunnen vertalen, zie \ref{sec:defining_rules}.
	\item optioneel heeft een type een $description$ tag: deze bezit een NL tekststring die aangeeft hoe een element van dat type in NL ge\"interpreteerd dient te worden. Als er geen $description$ tag wordt gegeven, probeert het vertaalsysteem het element letterlijk met de typenaam te vertalen. In die tekststring wordt $\#$ als markeersymbool gebruikt om naar dat element zelf te verwijzen. Bv. indien een variabele met type $VCPer$ vertaald moet worden, bepaalt de $description$ tag dat de tekststring ''$\text{the \# Very Cool Person}$'' gebruikt moet worden voor de vertaling ervan. 
	\item optioneel heeft een type een annotatie voor de aritmetische operatoren $+$, $-$, $*$ of $/$ met een tekststring om een alternatieve vertaling daarvoor aan te geven. Dit kan enkel als het type zelf een 'wrapper' is voor het integere type door de 'isa int' notatie te gebruiken. Zo wordt er bijkomende betekenis gegeven aan numerieke waarde of aritmetische expressies met dat type. De tekststring zelf bevat markeersymbolen $\#X$ om naar de vertaling van een operand $X$ te verwijzen. Het kan bv. gewenst zijn om een type $Pos$ voor 'De positie van een voorwerp' als natuurlijk getal te beschrijven en $+$ te vertalen met ''$\text{lies \#1 more to the right than \#2}$'' in plaats van de standaard vertaling ''$\text{\#1 plus \#2}$''.
\end{itemize}

\noindent Een geannoteerde type in een vocabularium heeft de vorm:
\begin{align*}
\text{type T ([isa $T_2$]|[constructed from \{...\}]) $\#TYPE$ $[THING|ENTITY]$}\\
\text{($description$"some description \#'' $+$''something \#1 and \#2'' $-$''...'' $*$''...'' $/$''...'')}
\end{align*}

\noindent Een predicaat in een FO(.) vocabularium $\psi$ wordt geannoteerd met:
\begin{itemize}
	\item een $\#PREDICATE$ tag
	\item een $description$ tag: deze bezit een NL tekststring die aangeeft hoe een voorkomen van het predicaat in NL Engels ge\"interpreteerd wordt. In die woordstring wordt een markeersymbool met nummer $\#1$, $\#2$, etc... gebruikt om te verwijzen naar (de interpretatie van) het overeenkomstige argument van het predicaat. 
	\item optioneel kan er een $\neg description$ tag worden gegeven om de NL interpretatie van de negatie van het predicaat aan te geven. Zo kan bv. de negatie $\neg ParentOf(John,Steve)$ dankzij een $\neg description$ tag met tekststring $\text{\#1 is not a parent of \#2}$ vertaald worden als ''$\text{John is not a parent of Steve}$'', wat natuurlijker klinkt dat ''$\text{It is not true that John is a parent of Steve}$'', waarbij enkel de $description$ vertaling gebruikt wordt.
\end{itemize}

\noindent Een geannoteerd predicaat in een vocabularium heeft de vorm:
\begin{align*}
\text{P($T_1$,...,$T_k$) $\#PREDICATE$ $description$''Some description \#1 where ... and \#k''}\\ \text{($\neg description$''Some negative description \#1 where .... and \#k'')}
\end{align*}

\noindent Een functie of een constante in een FO(.) vocabularium $\psi$ wordt geannoteerd met:
\begin{itemize}
	\item een $\#FUNCTIE$ tag
	\item een $description$ tag die een \textit{beschrijvende NL interpretatie} voor een voorkomen van de functie geeft. Net zoals bij een predicaat gaat deze gepaard met een NL woordstring met markeersymbolen voor de argumenten. Er wordt een bijkomend markeersymbool $\#R$ gebruikt voor het outputargument van de functie. Zo kan bv. de expressie $FatherOf(John)=Dave$ dankzij de tekststring ''$\text{\#R is the father of \#1}$'' vertaald worden met ''$\text{Dave is the father of John}$''. Bij een constante functie kan de beschrijvende NL interpretatie gebruikt worden om een toekenning te beschrijven: bv. als $GravitationalConstant:int$ in het vocabularium zit, zal de FO(.) zin $GravitationalConstant=9.81.$ vertaald worden als $''\text{The gravitational constant is 9.81.}''$.
	\item een optionele $\neg description$ tag met dezelfde structuur als een $\neg description$ tag, om de NL interpretatie van de functie aan te geven in een $\not=$ expressie. Zo kan bv. de expressie $IsParent(John)\not=Dave$ dankzij de tekststring ''$\text{\#R is not the father of \#1}$'' vertaald worden met ''$\text{Dave is not the father of John}$'', wat natuurlijker klinkt dan ''$\text{not Dave is the father of John}$'' waarbij enkel de $description$ vertaling gebruikt wordt. Bij een constant functie kan de objectieve beschrijving gebruikt worden om de 'betekenis' van een constante te vertalen: bv. een voorkomen van $GravitationalConstant$ wordt dan vertaald met $''\text{the gravitational constant}''$, wat natuurlijker klinkt dan een waarde $9.81$.
	\item een verplichte $=$ tag die de \textit{object NL interpretatie} voor een voorkomen van de functie geeft. Deze vertaling is noodzakelijk wanneer de functie als 'object' of doel van een expressie voorkomt. Een $=$ tag gaat gepaard met een NL woordstring waarbij enkel markeersymbolen met nummers $\#X$ gebruikt worden om naar de interpretaties van de functieargumenten te verwijzen, niet naar de interpretatie van het outputargument. Zo moet bv. in de expressie $FatherOf(John)=FatherOf(Dave)$ de eerste $FatherOf$ met de $description$ vertaling vertaald worden en de tweede (rechtse) $FatherOf$ met de $=$ vertaling. Wanneer de $=$ tag als tekststring ''$\text{the father of \#1}$'' heeft, wordt de vertaling ''$\text{the father of Dave is the father of John}$''. Merk op dat bij een constante functie zowel $=$ als $description$ dezelfde interpretatie kunnen hebben wanneer bij $description$ het markeersymbool voor het outputargument niet wordt gebruikt.
	\end{itemize}

\noindent Een geannoteerde functie in een vocabularium heeft de vorm:
\begin{align*}
\text{F($T_1$,...,$T_k$):$T_r$ $\#FUNCTION$ $description$''Some description \#1 where ... and \#k and \#R''}\\ \text{($\neg description$''Some negative description \#1 where .... and \#k and \#R'')}\\
\text{ $=$''Some description where \#1.... and \#k''}
\end{align*}

\noindent Een geannoteerde constante in een vocabularium heeft de vorm:
\begin{align*}
\text{C:$T_r$ $\#FUNCTION$ $description$''Some description \#R''}\\
\text{($\neg description$''Some negative description and \#R'') $=$''Some description''}
\end{align*}


\section{Typeinferentie}
\label{sec:inference}

FO(.) is een getypeerde logica, en het vertaalsysteem moet daarmee rekening houden. Het type van een expressie bevat immers belangrijke informatie over hoe ze in NL ge\"interprereerd dient te worden. Deze sectie legt in het algemeen uit hoe het vertaalsysteem typeinformatie aanpakt. Een manier om dit te doen is om het type van elke deelexpressie van een FO(.) formule te bepalen. Voor de formele beschrijving van de vertaalregels in \ref{sec:defining_formalism} wordt verondersteld dat het type van elke deelexpressie beschikbaar is. De implementatie van het vertaalsysteem voert een beperkte vorm van type inferentie uit, welke hierna kort beschreven wordt.

Om de vertaling van een predicaat of functie te bepalen, kan het vertaalsysteem gewoon de juiste vocabularium annotatie raadplegen. De types van de argumenten of het outputargument bij een functie worden eveneens uit het vocabularium afgeleid. Constanten en (gekwantificeerde) variabelen hebben een type dat afgeleid moet worden voorafgaand aan het vertalen van de FO(.) formule. Deze types worden bijgehouden in een tabel. Het vertaalsysteem werkt hiervoor in twee stappen: in elke stap wordt de abstracte syntaxboom (corresponderend met de te vertalen FO(.) formule) een keer doorlopen. In de eerste stap wordt aan typeinferentie gedaan en de types bijgehouden in de tabel. In de tweede stap wordt de vertaling zelf opgesteld door onder andere gebruik te maken van deze tabel.

Het type van een gekwantificeerd variabele kan expliciet gegeven zijn, of afgeleid worden uit de argumentpositie bij een functie- of predicaatsymbool waar het voorkomt. Wanneer de variabele, constante of numerieke waarde voorkomt in een aritmetische uitdrukking is het type sowieso 'integer' of 'natuurlijk', maar de NL interpretatie hangt af van de context waar die integer in voorkomt: komt de uitdrukking voor in een (on)gelijkheid en staat er een functie in de andere operand, dan moet bij de vertaling van die aritmetische uitdrukking het outputtype van die functie gebruikt worden i.p.v. integer of natuurlijk. Hetzelfde geldt wanneer de aritmetische expressie voorkomt als argument van een functie of predicaat: daar wordt dan het corresponderende argumenttype gebruikt.

Het type van een constantesymbool kan eveneens afgeleid worden uit de manier waarop het bij een predicaat of functie gebruikt wordt. Constanten kunnen in het vocabularium bij een type gedefeni\"eerd worden of als een constante functie. In beide gevallen kan het type uit het vocabularium verkregen worden.

\section{Vertalen volgens AST, compositie en contextpropagatie}
\label{sec:hollistic-view}
In deze sectie wordt een holistisch perspectief gegeven op de werking van het vertaalsysteem. Ter verduidelijking worden er een aantal voorbeelden gegeven. Nadat de AST van de FO(.) specificatie geparset is wordt elke gevonden FO(.) theorie en structuur vertaald. Hiervoor wordt gebruik gemaakt van het relevante vocabularium met annotaties.\\

\noindent Een \textit{vertaling} wordt opgesteld door:

\begin{itemize}
	\item de nodes in de \textit{syntaxboom} te doorlopen vanaf de root (de hele FO(.) specificatie)
	\item waarbij \textit{contextinformatie} van een oudernode naar de dochternodes gepropageerd wordt 
	\item waarbij de \textit{parti\"ele vertaling} opgesteld op het niveau van de dochternodes naar de ouder node gepropageerd wordt.
	\item De exacte manier waarop dit gebeurt, wordt beschreven door \textit{de vertaalregels} die gelden op het niveau van die oudernode. In \ref{sec:defining_rules} worden de verschillende vertaalregels formeel beschreven.
\end{itemize}

\begin{figure}
	\begin{framed}
		\caption{Algoritmische beschrijving van het vertaalsysteem.}
		\label{fig:algorithmic-description}
		\begin{algorithmic}[1]
			\Require de AST van een FO(.) specificaties met een theorie $T$ en structuur $S$, beiden over een vocabularium $V$
			\Require elke uitdrukking $U$ in $T$ of $S$ is een node [kind:soort, label:naam, content:(dochter\_node\_1,...,dochter\_node\_k)] uit de AST
			\Procedure{vertaal\_specificatie}{specificatie} 
			\For{$U$ in specificatie}
			\State vertaal\_context $\gets$ \{\}
			\State \textbf{print} vertaal\_uitdrukking($U$,vertaal\_context)
			\EndFor
			\EndProcedure
		\end{algorithmic}
		\begin{algorithmic}[1]
		\Function{vertaal\_uitdrukking}{[kind, label, content], context}
		\State vertaalregel $\gets$ bepaal\_vertaalregel(kind,label,context)
		\State dochter\_vertalingen $\gets$ \{\}
		\For{$D$ in content}
		\State dochter\_context $\gets$ bepaal\_context(vertaalregel,context,$D$)
		\State dochter\_vertaling[$D$] $\gets$ vertaal\_uitdrukking($D$, dochter\_context)
		\EndFor
		\State vertaling $\gets$ vertaal\_met\_regel(vertaalregel, dochter\_vertalingen)\\
		\Return vertaling
		\EndFunction
		\end{algorithmic}
	\end{framed}
\end{figure}

Wanneer de huidige node die vertaald wordt correspondeert met een zin in een FO(.) theorie, moet er eerst type inferentie uitgevoerd worden. Een \textit{vertaalregel voor een FO(.) formule} bepaalt hoe de vertaling van de verschillende deelexpressies gecombineerd wordt en waar welke (Engelse) tekst wordt bijgeplaatst. Elke afzonderlijke FO(.) formule wordt als een op zichzelf staande zin vertaald. Bv. de definitie $$ \forall x,y: ParentOf(x,y) \leftarrow ChildOf(y,x).$$ wordt vertaald als $$''\text{A first person is the parent of a second person when that second person is the child of that first person.}''$$

De vertaalregels maken gebruik van de informatie die in het vocabularium geannoteerd staat. Welke vertaalregel in een node gebruikt wordt, hangt af van de syntactische structuur van die node alsook de opgestelde contextinformatie. Dit is specifiek het geval bij het vertalen van een FO(.) formule. Hieronder volgt een lijst met de verschillende \textit{vertaalcontext symbolen} en een voorbeeld van hoe en waarom ze gebruikt worden. De formele beschrijving van vertaalregels zal hun gebruik verder specificeren.

\begin{itemize}
	\item $\forall x$: geeft aan dat $x$ een variabele is die 'voor alle' gekwantificeerd.
	\item $\exists* x$: geeft aan dat $x$ een variabele is die 'er bestaat exact/meer dan (of gelijk aan)/ minder dan (of gelijk aan) X' gekwantificeerd is. de $*$ geeft dit aan met $=X$,$>X$,$<X$,$\geq X$ of $\leq X$. Komt er voor een $x$ geen $\forall$ of $\exists$ argument in de context, wordt $x$ binnen die context als een constantesymbool beschouwd. Voor een gekwantificeerde $x$ zit steeds enkel $\forall x$ of $\exists x$ in de context, en dit zal steeds de meest recente kwantificering zijn als $x$ opnieuw wordt gekwantificeerd.
	Bv. beschouw een FO(.) theorie over familiestructuren (zie appendix \ref{app:B} voor de volledige specificatie): bij $$\exists x: parentOf(x,John).$$ wordt $x$ vertaald als een verwijzing naar een $Person$ en $John$ als een constante. De vertaling is dan $$''\text{Some person is the parent of John.}''$$ 
	\item $\neg$: geeft aan dat de te vertalen deelformule binnen een nog onvertaalde \textit{negatiecontext} voorkomt. Bv. bij de vertaling van $$\neg(ParentOf(John,Dave)).$$ zal de $\neg description$ vertaling uit de annotatie van $ParentOf$ in het vocabularium gebruikt worden. De vertaling wordt dan $$''\text{John is not a parent of Dave.}''$$ in plaats van $$''\text{it is not true that John is a parent of Dave.}''$$
	\item $ignore\neg$: geeft aan dat de volgende te vertalen negatie niet vertaald moet worden. Bv. Bij de formule $$\forall x: \neg(parentOf(x,x)).$$ zal bij de vertaling van de $\forall$ de negatie worden opgelost door een tekststring voor de vertaling van de rest van $\neg(parentOf(x,x)$ te zetten. die negatie zal zelf niet vertaald worden omdat ze $ignore\neg$ in haar context heeft. De vertaling wordt dan $$''\text{It doesn't hold that x is a parent of himself.}''$$
	\item $R=x$: geeft aan dat $x$ recursief voorkomt en met $itself$ of $himself$ vertaald moet worden, afhankelijk of het type van $x$ in het vocabularium geannoteerd werd met een $THING$ of $ENTITY$ tag. Bv. bij $$\neg Parentof(John,John).$$ is de tweede $John$ recursief. De vertaling wordt dan $$''\text{John is not a parent of himself.}''$$
	\item $=$: geeft aan dat een te vertalen functie moet vertaald worden als een object i.p.v. met haar descriptieve vertaling. in \ref{sec:voc_annotations} werd reeds gesteld dat functies in een vocabularium met twee verschillende (positieve) vertalingen geannoteerd werden, met label $description""$ en $=""$. Bv. bij de vertaling van $$ParentOf(Jos)=ParentOf(Peter).$$ zal de linkse $ParentOf$ met de descriptieve vertaling vertaald worden de rechtse met de object vertaling. De vertaling wordt dan $$''\text{The father of John is the father of Diana.}''$$ in plaats van $$''\text{The father of John is the father of Diana is.}''$$ wanneer beiden hun descriptieve vertaling zouden krijgen.
	\item $F=TRANSLATION$: geeft aan dat $TRANSLATION$ het reeds vertaalde resultaat is van een functie die vertaald wordt. Bv. bij de vertaling van $$FatherOf(John)=Peter.$$ wordt dus eerst $Peter$ (als zichzelf) vertaald en $F=Peter$ in de context voor de vertaling van $FatherOf$ meegegeven. De vertaling wordt dan $$''\text{The Father of John is Peter.}''$$
	\item $\Rightarrow P$: geeft aan dat de expressie in de huidige context vertaald moet worden zoals in de premisse van een implicatie.
	\item $\Rightarrow C$: geeft aan dat de expressie in de huidige context vertaald moet worden zoals in de conclusie van een implicatie. $\Rightarrow P$ en $\Rightarrow C$ kunnen niet tegelijk in de context zitten. Bv. bij de vertaling van $$ \forall x: ParentOf(x,Elma) \Rightarrow ChildOf(Elma,x) $$ zal de context $\Rightarrow P$ bevatten bij de vertaling van de $ParentOf$ en een $\Rightarrow C$ bij de vertaling van de $ChildOf$. Dit is bepalend voor de determinant en de verwijswoorden bij $x$: de vertaling wordt $$''\text{If a person is the parent of Elma, then Elma is the child of that person.}''$$
\end{itemize}

De vertaling van een FO(.) theorie of structuur in zijn geheel zal de nodige \textit{lijmtekst} toegevoegd worden aan de vertaling van wat er in de sectie zelf staat.

Bv. $$\text{theory T:V } \{\forall x y: ParentOf(x,y) \Rightarrow ChildOf(y,x).\} $$ wordt vertaald als:
\begin{align*}
\text{Translation of structure T over vocabulary V:}\\
\text{A first person is the parent of a second person if}\\
\text{that second person is the child of that first person.}
\end{align*}

De \textit{domeinbeschrijvingen} in een FO(.) structuur worden veelal via opsomming vertaald. Elke zulke beschrijving wordt als een op zichzelf staande zin vertaald.

Bv. $$\text{structure T:V} \{ParentOf<ct>={(John,Max);(John,D-+ave)}\}$$ wordt vertaald als:
\begin{align*}
\text{Translation of structure S over vocabulary V:}\\
\text{It is certainly true that John is a parent of Max and John is a parent of Dave.}
\end{align*}

\section{Besluit van dit hoofdstuk}
In dit hoofdstuk werd het gebruik van een geannoteerd vocabularium ge\"introduceerd. Er werd uitgelegd waarom het noodzakelijk is voor het vertaalsysteem om een expliciete interpretatie voor de verschillende vocabulariumsymbolen te hebben. De verschillende NL interpretatie tags voor types, functies en predicaten werden vastgelegd. Daarnaast werd uitgelegd hoe het vertaalsysteem in het algemeen werkt: de te vertalen FO(.) specificatie wordt als een abstracte syntaxboom voorgesteld. Er gelden vertaalregels op de verschillende niveaus in de boom. De vertaling wordt opgebouwd door per niveau de juiste vertaalregels te selecteren, afhankelijk van de vertaalcontext die wordt doorgegeven vanuit een oudernode en de door de deelvertalingen uit de dochternodes te combineren zoals de regel voorschrijft. De verschillende symbolen om die vertaalcontext te beschrijven werden ge\"introduceert en besproken.

%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "masterproef"
%%% End: 
