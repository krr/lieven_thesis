\chapter{FO(.) syntax}
\label{hoofdstuk:fodot-syntax}
In dit hoofdstuk wordt een overzicht gegeven van de FO(.) syntax die het vertaalsysteem hanteert. Zowel een FO(.) vocabularium, theorie en structuur worden hier beschreven. De semantiek van de beschreven symbolen wordt verondersteld zoals in \cite{de2014predicate}.

\section{Een FO(.) vocabularium}

Een \textit{FO(.) Vocabularium} $\psi$ is een verzameling van symbolen. De mogelijke symbolen zijn:
\begin{itemize}
	\item een typesymbool $T$, beschreven als '$\text{type } T$' en optioneel gevolgd door '$\text{isa }T_2$' om aan te geven dat $T$ ook een $T_2$ is of '$\text{constructed from }\{C_1,...,C_x\}$' om aan te geven dat de constantesymbolen $C_1,...,C_x$ als type $T$ hebben. Alternatief kan '$\text{type T} = \{N_1,...,N_x\}$' met '$\text{isa nat}$' of '$\text{isa int}$' gebruikt worden om aan te geven dat $T$ een numeriek type over $N_1,...,N_x$ is.
	\item een predicaatsymbool $P$, beschreven als $P(T_1,...,T_k)$, met ariteit $k$ en $T_1,...,T_k$ de types op de overeenkomstige argumentposities.
	\item een functiesymbool $F$, beschreven als $F(T_1,...,T_k)\colon T_r$, met ariteit $k$ en $T_1,...,T_k$ de types op de overeenkomstige argumentposities en $T_r$ het type van het functieresultaat. 
	\item een constantesymbool (of constante functie) $C$, beschreven als $C\colon T$ waarbij $T$ het type van $C$ is.
\end{itemize}


\section{Een FO(.) formule}
Een logische formule wordt opgebouwd in de context van een vocabularium $\psi$ en een veronderstelde set variabelesymbolen. Typisch wordt hiervoor $x$, $y$, etc... gebruikt. Een logische formule $\phi$ wordt opgebouwd met termen. Een \textit{term} $t$ is:
\begin{itemize}
	\item een constantesymbool $C \in \psi$.
	\item een variabelesymbool $x$.
	\item een functietoepassing $F(t_1,...,t_k)$ waarbij $F \in \psi$ en $k$ de ariteit van $F$.
	\item een aggregaat $card\{x\colon\phi\}$, $sum\{x,y,...\colon\phi\colon t\}$, $prod\{x,y,...\colon\phi\colon t\}$, $min\{x,y,...\colon\phi\colon t\}$, $max\{x,y,...\colon\phi\colon t\}$, waarbij $x$,$y$ een variabele is, $\phi$ een formule met $x,y,...$ als vrije variabelen en $t$ een term.
	\item de aritmetische expressie $t_1+t_2$, $t_1-t_2$, $t_1*t_2$, $t_1/t_2$, $t_1\%t_2$.
	\item de aritmetische expressie $-t_1$, $abs(t_1)$.
\end{itemize}

Een \textit{logische formule} $\phi$ is:
\begin{itemize}
	\item een predicaattoepassing $P(t_1,...,t_k)$ waarbij $P \in \psi$ en $k$ de ariteit van $P$.
	\item een expressie $t_1=t_2$,$t_1\not=t_2$,$t_1<t_2$,$t_1>t_2$,$t_1\leq t_2$,$t_1\geq t_2$.
	\item een expressie $\neg\phi$.
	\item een expressie $\phi_1\wedge\phi_2$, $\phi_1\vee\phi_2$, $\phi_1\Rightarrow\phi_2$, $\phi_1\Leftarrow\phi_2$, $\phi_1\Leftrightarrow\phi_2$.
	\item een expressie $\forall x\colon \phi$, $\exists x \colon \phi$, $\exists= k x \colon \phi$, $\exists\leq k x \colon \phi$, $\exists\geq k x \colon \phi$ met $k$ een natuurlijk getal.
\end{itemize}

\section{Een FO(.) definitie}

Een \textit{FO(.) definitie} is opgebouwd uit regels. Een \textit{regel} is een expressie van de vorm $P(t_1,...,t_k)\leftarrow\phi$ waarbij $P$ een predicaatsymbool uit $\psi$, met ariteit k, over termen $t_1$ tot $t_k$. Alle variabelesymbolen in een definitieregel moeten expliciet worden gekwantificeerd met een $\forall x_1,...,x_k$ voor \textit{het hoofd van de regel} $P(t_1,...,t_k)$ of een van de $\exists$ kwantoren voor \textit{het lichaam van de regel} $\phi$.

\section{Een FO(.) theorie}

Een \textit{logische zin} is een formule zonder vrije variabelen met een ''$.$'' aan het einde. Een variabele is \textit{niet vrij} wanneer ze gebonden wordt door een kwantor of aggregaatexpressie.\\

\noindent Een \textit{FO(.) theorie} over een vocabularium $\psi$ is een verzameling van logische zinnen en definities met symbolen uit $\psi$.

\section{Een FO(.) structuur}

Een \textit{FO(.) structuur} $S$ over vocabularium $\psi$ geeft een interpretatie aan alle types en aan (sommige) symbolen uit $\psi$. De mogelijke interpretaties in een structuur $S$ zijn:

\begin{itemize}
\item een interpretatie $S(T)$ voor een type $T$ is een verzameling elementen, die we de \textit{domeinelementen} noemen.
\item een interpretatie voor een predicaatsymbool $P(T_1,...T_k)$ is een relatie over het k-dimensioneel cartesisch product $S(T_1)\times...\times S(T_k)$. Met andere woorden, een interpretatie voor een predicaatsymbool is een verzameling k-tupels over de domeinelementen van de corresponderende types.
\item een interpretatie voor een functiesymbool $F(T1,...Tk):Tr$ is een functie van het k-dimensioneel cartesisch product $S(T_1)\times ...\times S(T_k)$ naar $S(T_r)$. Met andere woorden, een interpretatie voor een functiesymbool is een verzameling (k+1)-tupels $(d_1,...,d_k\rightarrow d_r)$ over de domeinelementen van de corresponderende types, die voldoet aan de voorwaarde dat voor elk element $(d_1,...,d_k)$ uit $S(T_1)\times...\times S(T_k)$ er exact 1 tupel bestaat die $(d_1,...,d_k)$ uitbreidt met een $d_r \in S(T_r)$.
\item een interpretatie voor een constantesymbool $C:T$ is een element uit $S(T)$.
\end{itemize}

\noindent Bij het specificeren van structuren wordt ook toegelaten dat interpretaties van symbolen (niet van types) \textit{partieel} zijn, in de zin dat sommige tupels zeker wel ($<ct>$, ''certainly true''), zeker niet ($<cf>$, certainly false''), of misschien ($<u>$ ''unknown'') ertoe behoren.

%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "masterproef"
%%% End: 
