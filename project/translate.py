#!/usr/bin/env python

# This program generates natural language English translations of FO(.) logic expression and other parts of an IDP model.
#
# Copyright 2017, by Lieven Paulissen

import argparse
import sys
import time
from timeit import default_timer as timer
from collections import OrderedDict

from pyparsing import infixNotation, opAssoc, Keyword, Word, alphas, alphanums, printables,\
    ZeroOrMore, OneOrMore, Group, Optional, FollowedBy, SkipTo, delimitedList, Literal, Suppress, ParseException,\
    Forward, restOfLine, lineEnd, QuotedString, oneOf, ParserElement
from compiler.ast import Function
from _ctypes_test import func
    
ParserElement.enablePackrat()

# translating a node in the parse tree happens:
#       1) compositional bottom-up using local information
#       2) based on information from the relevant vocabulary parse and type inference, passed as a parameter 
#       of the .translate() call
#       3) based on context constraints made higher in the parse tree, also passed as a parameter of 
#          the .translate() call 
#       when returning from a .translate() call, the translation of that node should be final

# AST structure:
# every node has:
#       - a label: the name of the content in the node
#       - a content: a list with the node's contents
#       - a kind: the type/kind of content stored in the node
# some nodes also have:
#       - a vars: a list of arguments used by the node content
#       - a annotation: gives extra info about the translation of the node


# translation happens in two steps: after a parse tree has been constructed, type inference is run to determine the types of all arguments 
# (these are: expression arguments and (possibly nested) arguments of functions and predicates)
# in the second step, a NL translation is generated of the different parts of the IDP model.
# the translation rules that are used, are described at the implementation of the translate() function on the level at which they are relevant.
# e.g. the definition of translate() for the '+' operator (parsed by the ArithmeticLeftBinOp class) implements the '+' translation rules.

# NOTES:
# The program will complain if the model contains a syntax error or unsupported syntax.
# The model is not checked for logical or semantical errors: this will lead to incorrect or incomplete translations.
# arguments in a list must be seperated by comma's, not spaces
# constants should start with a capital letter
# multiline comments are not allowed
# not-exists and not-forall are not allowed, only positive quantifications
# only theories and structures are translated
# don't use more than 5 quantified variables of the same type in a sentence
# don't use overlapping function/predicate names
# don't re-use variable names in a logic formula
# % and abs() are not supported
# only binary comparisons are supported: A=B is allowed but not A=B=C
# An NL interpretation string annotated with the vocabulary can only consist of words, argument identifiers and "," or "'"
# only ?=1 is supported, not ?=x,?>x, etc...

# BEGIN OF PROGRAM

# define some language vocabulary mappings

reference_map = {
    1:"first",
    2:"second",
    3:"third",
    4:"fourth",
    5:"fifth",
    }
    
    
# define the classes that represent the different node types in the parse tree

class QuantorOperand(object):
    def __init__(self,t):
        self.label = t[0]
        self.vars = t[1:-1]
        self.content = t[-1]
        self.kind = "quantifier"
    def ast(self):
        return {'Label':self.label,'Arguments':self.vars,'Content':self.content.ast(),'Kind':self.kind}
    def infer(self,vocParse):
        result = OrderedDict()
        #check if the type of a variable was explicitly declared
        for i in self.vars:
         if len(i)==2:
          result[i[0]]=i[1]
        result.update(self.content.infer(vocParse))
        return result
    def translate(self,vocParse,types,*args):
        vartypes = [x[0] for x in list(self.vars)]
        for i in range(0,len(vartypes)):
         if vartypes[i] in types:
          vartypes[i] = reference_map[types[vartypes[i]][1]] + " " + types[vartypes[i]][0]
        # construct the quantifier string
        varstring=""
        if len(vartypes) == 1:
         varstring = vartypes[0] + " "
        else:
         for i in vartypes[0:-1]:
          varstring += i + ", "
         varstring += vartypes[-1]+" "
        # translate the quantification 
        if self.content.label != '~':
         holdstring = "it holds that "
         if self.content.label == '!' or self.content.label == '?':
          holdstring = "and "
         if self.label == '!':
          newargs = args[0] + ["#!"+i[0] for i in self.vars]
         elif self.label == '?':
          newargs = args[0] + ["#?"+i[0] for i in self.vars]
         elif self.label == '?=1':
          newargs = args[0] + ["#?=1"+i[0] for i in self.vars]
         return self.content.translate(vocParse,types,newargs)
        else:
         holdstring = "it doesn't hold that "
         if self.content.label == '!' or self.content.label == '?':
          holdstring = "and "
         if self.label == '!':
          newargs = args[0] + ["ignore-not"] + ["#!"+i[0] for i in self.vars]
         elif self.label == '?':
          newargs = args[0] + ["ignore-not"] + ["#?"+i[0] for i in self.vars]
         elif self.label == '?=1':
          newargs = args[0] + ["ignore-not"] + ["#?=1"+i[0] for i in self.vars]
         return holdstring + self.content.translate(vocParse,types,newargs)
class FunctionOperand(object):
    def __init__(self,t):
        self.label = t[0]
        self.content = t[1:]
        self.kind = "vocsymbol"
    def ast(self):
        if len(self.content)==0:
            return {'Label':self.label,'Kind':self.kind}
        else:
            return {'Label':self.label,'Content':[i.ast() for i in self.content],'Kind':self.kind}
    def infer(self,vocParse):
        result = OrderedDict()
        if len(self.content) != 0:
         # determine the types of the predicate/function arguments
         for i in vocParse.content:
          if (i.kind == "predicate" or i.kind == "function") and i.label == self.label:
           for j in range(0,len(self.content)):
            # if one of the arguments is a nested term, do a recursive infer call
            if len(self.content[j].content) == 0:
             result[self.content[j].label] = i.content[j]
            else:
             result.update(self.content[j].infer(vocParse))
           if i.kind == "function" and len(self.content) == 0:
            # if the operand is itself a constant symbol, infer the result type as its type
            result[self.label] = i.content[0]
        return result
    def translate(self,vocParse,types,*args):
        translation = None
        
        # determine whether this symbol is a (constant) function or predicate in the vocabulary
        # otherwise it is treated as a constant (starts with capital letter) of variable (starts with small letter)
        VarOrConstant = True
        for i in vocParse.content:
         if (i.kind == "predicate" or i.kind == "function") and i.label == self.label:
          VarOrConstant = False
          break
          
        # check whether to translate the operand as a typed variable or constant
        if VarOrConstant:
         
         if "#R="+self.label in args[0]:
          # the operand occurs in a recursive context, use 'himself/itself'
          for i in vocParse.content:
           if i.kind == "type" and i.label == types[self.label][0]:
            for j in i.annotation.content:
             if j.kind == "annotation-labeltag" and j.label=="THING":   
              translation = "itself"
              if j.kind == "annotation-labeltag" and j.label=="ENTITY": 
               translation = "himself"
         
         elif self.label in types and not self.label[0].isupper():
          # interpret the operand as a variable
          
          # check type table for other variables (start with small letter) of the same type
          sametypenoconstant = [l for l in types.keys() if types[l][0]==types[self.label][0] and not l[0].isupper()]
          if types[self.label][2] == False:
           # treat the operand as a quantified variable (with a inferred type in types)
           # use referring quantifiers and the type name for translation
           if "#=>P" in args[0]:
            if "#!"+self.label in args[0] and len(sametypenoconstant) == 1:
             if types[self.label][0][0] in ['a','e','i','o','u']:
              translation = "an " + types[self.label][0]
             else:
              translation = "a " + types[self.label][0]
            elif "#!"+self.label in args[0]:
             translation = "a " + reference_map[types[self.label][1]] + " " + types[self.label][0]
            elif "#?"+self.label in args[0] and [l[0] for l in types.values()].count(types[self.label][0]) == 1:
             translation = "some " + types[self.label][0]
            elif "#?"+self.label in args[0]:
             translation = "some " + reference_map[types[self.label][1]] + " " + types[self.label][0]
            elif "#?=1"+self.label in args[0] and [l[0] for l in types.values()].count(types[self.label][0]) == 1:
             translation = "exactly one " + types[self.label][0]
            elif "#?=1"+self.label in args[0]:
             translation = "exactly one " + reference_map[types[self.label][1]] + " " + types[self.label][0]
           elif "#=>C" in args[0]:
            if "#!"+self.label in args[0] and len(sametypenoconstant) == 1:
             translation = "any " + types[self.label][0]
            elif "#!"+self.label in args[0]:
             translation = "any " + reference_map[types[self.label][1]] + " " + types[self.label][0] 
            elif "#?"+self.label in args[0] and [l[0] for l in types.values()].count(types[self.label][0]) == 1:
             translation = "some " + types[self.label][0]
            elif "#?"+self.label in args[0]:
             translation = "some " + reference_map[types[self.label][1]] + " " + types[self.label][0]
            elif "#?"+self.label in args[0] and [l[0] for l in types.values()].count(types[self.label][0]) == 1:
             translation = "exactly one " + types[self.label][0]
            elif "#?"+self.label in args[0]:
             translation = "exactly one " + reference_map[types[self.label][1]] + " " + types[self.label][0]
           else:
            if "#!"+self.label in args[0] and len(sametypenoconstant) == 1:
             translation = "every " + types[self.label][0]
            elif "#!"+self.label in args[0]:
             translation = "every " + reference_map[types[self.label][1]] + " " + types[self.label][0]
            elif "#?"+self.label in args[0] and [l[0] for l in types.values()].count(types[self.label][0]) == 1:
             translation = "some " + types[self.label][0]
            elif "#?"+self.label in args[0]:
             translation = "some " + reference_map[types[self.label][1]] + " " + types[self.label][0]
            elif "#?=1"+self.label in args[0] and [l[0] for l in types.values()].count(types[self.label][0]) == 1:
             translation = "exactly one " + types[self.label][0]
            elif "#?=1"+self.label in args[0]:
             translation = "exactly one " + reference_map[types[self.label][1]] + " " + types[self.label][0]
           # mark argument as occurred.
           types[self.label][2] = True
          else:
           # treat the operand as a typed variable that already occurred earlier
           if len(sametypenoconstant) == 1:
            translation = "that " + types[self.label][0]
           else:
            translation = "that " + reference_map[types[self.label][1]] + " " + types[self.label][0]
         
         elif self.label[0].isupper():
          # interpret the operand as a constant
          # use the description annotation tag of the type for translation (if available)
          intendedArgTranslation = ["#"]
          translation = ""
          for l in vocParse.content:
           if l.kind == "type" and l.label == types[self.label][0]:
            for m in l.annotation.content:
             if m.kind == "annotation-nlstringtag" and m.label == "description":
              intendedArgTranslation = m.content.content
              break
          for l in intendedArgTranslation:
           if l == "#":
            translation += self.label + " "
           else:
            translation += l + " "
          translation = translation[:-1]
          # mark argument as occurred.
          types[self.label][2] = True
        
        # check whether to translate the operand as a (negative) function or predicate
        else:
         for i in vocParse.content:
          if (i.kind == "predicate" or i.kind == "function") and i.label==self.label:
           # translate the predicate/function as a (negated) description or as an object
           translationtype = "description"
           if "#=" not in args[0] and "not" in args[0]:
            translationtype = "~description"
           elif "#=" in args[0]:
            translationtype = "="
           # check if the negative descriptive/objective translation is defined, 
           # otherwise we prepend a 'not' to the translation and fall back to the positive descriptive/object translation
           if translationtype == "~description":
            for j in i.annotation.content:
             if j.kind == "annotation-nlstringtag" and j.label == translationtype:
              translation=""
              break
            if translation==None:
             translation = "not "
             if translationtype == "~description":
              translationtype = "description"
           else:
            translation = ""
           # translate and combine the different parts of the predicate/function
           for j in i.annotation.content:
            if j.kind == "annotation-nlstringtag" and j.label == translationtype:
             temp = list(j.content.content)
             
             # keep a list of the argument and result types of the vocsymbol
             ty = i.content
                            
             # keep track of possible recursive references
             recursiveArg = []
              
             if translationtype == "description" or translationtype == "~description":              
              i=0
              # translate the symbol using the descriptive translation
              for k in range(0,len(temp)):
               if temp[k] == "#R":
                # the translation of the result is provided as a context argument
                for element in args[0]:
                 if element.startswith("#O="):
                  temp[k] = element[3:]
                  break
                 
               elif temp[k].startswith("#"):
                # select the right argument
                i = int(temp[k][1:]) - 1
                 
                # build a new argument context for the recursive translation call
                # by removing the 'typed context', 'objective translation context' and 'not scope context'
                newargs = []
                for el in args[0]:
                 if not el.startswith("#T=") and el !="#=" and el != "not":
                  newargs.append(el)
                
                # add the argument type to the context
                # this 'type context' argument is used in ValueOperand, where the operand type might be ambiguous
                argumentType = "#T=" + ty[i]  
                newargs.append(argumentType)
                 
                # the nested arguments will always use their objective translation, so add it to the new context
                newargs.append("#=")
                
                # append any previously detected recusive arguments
                newargs += recursiveArg
                 
                temp[k] = self.content[i].translate(vocParse,types,newargs)
                
                #in case the argument is a constant (function) or variable, add it to the recursive context
                if self.content[i].kind == "vocsymbol" and len(self.content[i].content)==0:
                 recursiveArg.append("#R="+self.content[i].label)
                 
             elif translationtype == "=":
              # translate the symbol with the 'assignment' translation
              for k in range(0,len(temp)):   
               if temp[k].startswith("#"):
               # select the right argument
                i = int(temp[k][1:]) - 1
                 
                # build a new argument context for the recursive translation call
                # by removing the 'typed context', 'objective translation context' and 'not scope context'
                newargs = []
                for el in args[0]:
                 if not el.startswith("#T=") and not el.startswith("#=") and el != "not":
                  newargs.append(el)
                
                # add the argument type to the context
                # this 'type context' argument is used in ValueOperand, where the operand type might be ambiguous
                argumentType = "#T=" + ty[i]  
                newargs.append(argumentType)
                 
                # the nested arguments will always use their objective translation, so add it to the new context
                newargs.append("#=")
                
                # append any previously detected recusive arguments
                newargs += recursiveArg
                 
                temp[k] = self.content[i].translate(vocParse,types,newargs)
                
                #in case the argument is a constant (function) or variable, add it to the recursive context
                if self.content[i].kind == "vocsymbol" and len(self.content[i].content)==0:
                 recursiveArg.append("#R="+self.content[i].label)
                 
             # finally, combine the translated fragments
             for i in temp[0:-1]:
              translation += i + " "
             translation += temp[-1]
             
        # check whether to go for a literal translation
        if translation == None:
         if len(self.content)==0:
          translation = self.label
         elif len(self.content)==1:
          translation = self.label+"("+self.content[0].label+")"
         else:
          translation = self.label+"("
          for i in self.content[0:-1]:
           translation += i+","
          translation += self.content[-1].label+")"
        return translation 
        
class NoTermAggregateExpr(object):
    def __init__(self,t):
        self.label = t[0]
        self.vars = t[1:2]
        self.content = [t[2]]
        self.kind = "notermaggregate"
    def ast(self):
        return {'Label':self.label,'Arguments':self.vars,'Content':[i.ast() for i in self.content],'Kind':self.kind}
    def infer(self,vocParse):
        result = OrderedDict()
        #check if the type of a variable was explicitly declared
        for i in self.vars:
         if len(i)==2:
          result[i[0]]=i[1]
        result.update(self.content[0].infer(vocParse))
        return result
    def translate(self,vocParse,types,*args):
     if self.label == 'card' or self.label == '#':
      univ = []
      varstring = ""
      for pos in range(0,len(self.vars)):
       # mark the variable as universally quantified in the context
       univ.append("!"+self.vars[pos][0])
       # set the variable to 'already referenced' for a better translation
       types[self.vars[pos][0]][2] = True
       # construct a variable reference string
       varstring += types[self.vars[pos][0]][0] + " "
       if pos != len(self.vars)-1:
        varstring += "and "
       varstring = varstring[:-1]
       # construct a new context, where the 'typed' contex, not scope and object translation scope are removed
       # the formula inside the aggregate is translated using the descriptive translation
       newargs = []
       for el in args[0]:
        if not el.startswith("#T=") and el != "not" and el != "#=":
         newargs.append(el)
       newargs += univ
      # return the final translation
      return "the number of " + varstring + ", where " + self.content[0].translate(vocParse,types,newargs) + ","
     
class TermAggregateExpr(object):
    def __init__(self,t):
        self.label = t[0]
        self.vars = t[1:2]
        self.content = [t[2]]
        self.term = t[3]
        self.kind = "termaggregate"
    def ast(self):
        return {'Label':self.label,'Arguments':self.vars,'Content':[i.ast() for i in self.content],'Term':self.term.ast(),'Kind':self.kind}
    def infer(self,vocParse):
        result = OrderedDict()
        #check if the type of a variable was explicitly declared
        for i in self.vars:
         if len(i)==2:
          result[i[0]]=i[1]
        result.update(self.content[0].infer(vocParse))
        return result
    def translate(self,vocParse,types,*args):
        univ = []
        varstring = ""
        for pos in range(0,len(self.vars)):
         # mark the variable as universally quantified in the context
         univ.append("!"+self.vars[pos][0])
         # construct a variable reference string
         varstring += types[self.vars[pos][0]][0] + " "
         if pos != len(self.vars)-1:
          varstring += "and "
         varstring = varstring[:-1]
         # construct a new context, where the 'typed' context, not scope and object translation scope are removed
         # the formula inside the aggregate is translated using the descriptive translation
         newargs = []
         for el in args[0]:
          if not el.startswith("#T=") and el != "not" and el != "#=":
           newargs.append(el)
         newargs += univ

        if self.label == 'sum':
         return "the sum over " + self.term.translate(vocParse,types,newargs) + ", where " + self.content[0].translate(vocParse,types,newargs)  + ","
        elif self.label == 'min':
         return "the minimum of " + self.term.translate(vocParse,types,newargs)  + ", where " + self.content[0].translate(vocParse,types,newargs)  + ","
        elif self.label == 'max':
         return "the maximum of " + self.term.translate(vocParse,types,newargs)   + ", where " + self.content[0].translate(vocParse,types,newargs)  + ","
        elif self.label == 'prod':
         return "the product over " + self.term.translate(vocParse,types,newargs)  + ", where " + self.content[0].translate(vocParse,types,newargs) + ","
        
class ValueOperand(object):
    def __init__(self,t):
        self.label = t[0]
        self.content = []
        self.kind = "value"
    def ast(self):
        return {'Label':self.label,'Content':self.content,'Kind':self.kind}
    def infer(self,vocParse):
        return OrderedDict()
    def translate(self,vocParse,types,*args):
        for s in args[0]:
         if s.startswith("#T="):
            for l in vocParse.content:
             if l.kind == "type" and l.label == s[3:]:
              # check for a description annotation, otherwise use default interpretation
              for n in l.annotation.content:
               if n.kind == "annotation-nlstringtag" and n.label == "description":
                intendedArgTranslation = n.content.content
                translation = ""
                for l in intendedArgTranslation:
                 if l == "#":
                  translation += self.label + " "
                 else:
                  translation += l + " "
                return translation[:-1]
              # without a description, use a default translation based on the wrapper type
              return l.label + " " + self.label
        # otherwise, interpret the value as a simple int
        return self.label

class LogicOperand(object):
    def __init__(self,t):
        self.label = t[0]
        self.content = t[1:]
        self.kind = "boolean"
    def ast(self):
        return {'Label':self.label,'Content':self.content,'Kind':self.kind}
    def infer(self,vocParse):
        return OrderedDict()
    def translate(self,vocParse,types,*args):
        return self.label

class DefinitionExpr(object):
    def __init__(self,t):
        self.label = "<-"
        self.content = t
        self.kind = "definition-rule"
    def ast(self):
        return {'Label':self.label,'Content':[i.ast() for i in self.content],'Kind':self.kind}
    def infer(self,vocParse):
        result = OrderedDict()
        
        # update result with inferred types from operands, but prefer non-integer types
        for i in self.content:
         l = i.infer(vocParse)
         for j in l.keys():
          if (j not in result.keys()) or (j not in result.keys() and l[j] != "Integer"):
           result[j] = l[j]   
        return result
    
    def translate(self,vocParse,types,*args):
        if len(self.content) == 2:
         if self.content[1].kind == 'boolean' and self.content[1].label == 'true':
          return self.content[0].translate(vocParse,types,args[0])
         elif self.content[1].kind == 'boolean' and self.content[1].label == 'false':
          return self.content[0].translate(vocParse,types,args[0] + ["not"])
         return self.content[0].translate(vocParse,types,args[0] + ["#=>P"]) + " when " + self.content[1].translate(vocParse,types,args[0] + ["#=>P"])
        else:
         return self.content[0].translate(vocParse,types,args[0])
      
class BinOp(object):
    def __init__(self,t):    
        self.label = t[0][1]
        self.content = t[0][0::2]
        self.kind = "binaryoperator"
    def ast(self):
        Content = [i.ast() for i in self.content]
        return {'Label':self.label,'Content':Content,'Kind':self.kind}
    def infer(self,vocParse):
        result = OrderedDict()
        
        # update result with inferred types from operands, but prefer non-integer types
        for i in self.content:
         l = i.infer(vocParse)
         for j in l.keys():
          if (j not in result.keys()) or (j not in result.keys() and l[j] != "Integer"):
           result[j] = l[j]
        return result

class ComparisonBinOp(BinOp):
    def infer(self,vocParse):
        result = OrderedDict()
        
        # in a comparison expression, all operands must have the same type
        # infer the type of a variable/constant operand from the other operand, if possible
        inferredType = None
        
        # do a recursive type inference call
        [result.update(i.infer(vocParse)) for i in self.content]
        
        # try inferring the comparison's type
        for el in self.content:
         if el.kind == "notermaggregate" or el.kind == "termaggregate" or el.kind == "value":
          inferredType = "Integer"
          break
         elif el.kind == "boolean":
          inferredType = "Boolean"
          break
         elif el.kind == 'vocsymbol':
          for i in vocParse.content:
           if i.kind == 'function' and i.label == el.label:
            inferredType = i.content[-1]
            break
          if inferredType != None:
           break
       
        #  if a type was inferred, update result for all variable/constant operands
        if inferredType != None:
         for el in self.content:
          if el.kind == 'vocsymbol' and len(el.content)==0 and el.label not in result.keys():
           result[el.label] = inferredType
           
        return result
    
    def translate(self,vocParse,types,*args):
        if self.label == '=':
            # if the left or right operand of the '=' expression is a function,
            # translate it using its descriptive translation and translate the other operand using its objective translation
            # in a negation context, translate the vocabulary function operand using a negated translation
            
            # if both operands are compound (arithmetic) expression, translate the '=' operator using 'equals'
            # if the '=' operator is in a negation context, translate it with 'is different from'
            
            # first check whether a symbol occurs inside a 'self reference' scope.
            # if one operand is a function/predicate (vocsymbol with content) and the other is a constant or a variable (vocsymbol without content)
            # check if that constant/variable already occured as an argument in the functionpredicate
            # if both operands are vocsymbols without content and are the same, the second occurence is treated as a self-reference
            # the self-reference check only searches for reference up to one nested level
            recursiveArg = []
            if self.content[0].kind == "vocsymbol" and len(self.content[0].content) != 0:
             if self.content[1].kind == "vocsymbol" and len(self.content[1].content) == 0:
              if self.content[1].label in [i.label for i in self.content[0].content]:
               recursiveArg.append("#R="+self.content[1].label)
              elif self.content[1].kind == "vocsymbol" and len(self.content[1].content) != 0:
               for i in self.content[0].content:
                if i.label in [j.label for j in self.content[1].content]:
                 recursiveArg.append("#R="+i.label)
            elif self.content[1].kind == "vocsymbol" and len(self.content[1].content) != 0:
             if self.content[0].kind == "vocsymbol" and len(self.content[0].content) == 0:
              if self.content[0].label in [i.label for i in self.content[1].content]:
               recursiveArg.append("#R="+self.content[1].label)
              elif self.content[0].kind == "vocsymbol" and len(self.content[0].content) != 0:
               for i in self.content[1].content:
                if i.label in [j.label for j in self.content[0].content]:
                 recursiveArg.append("#R="+i.label)
            elif self.content[0].kind == "vocsymbol" and self.content[1].kind == "vocsymbol" and len(self.content[0].content) == 0 and len(self.content[1].content) == 0:
             if self.content[0].label == self.content[1].label:
              recursiveArg.append("#R="+ self.content[0].label)
              

            if "not" not in args[0]:
             if self.content[0].kind == "vocsymbol" and len(self.content[0].content) !=0:
              #determine the return type of the function
              returnType = ""
              for i in vocParse.content:
               if i.kind=="function" and i.label==self.content[0].label:
                returnType = "#T="+i.content[-1]
              otheroperand = "#O=" + self.content[1].translate(vocParse,types,args[0] + recursiveArg + ["#=",returnType])
              return self.content[0].translate(vocParse,types,args[0] + [otheroperand])
             elif self.content[1].kind == "vocsymbol" and len(self.content[1].content) !=0:
              #determine the return type of the function
              returnType = ""
              for i in vocParse.content:
               if i.kind=="function" and i.label==self.content[1].label:
                returnType = "#T="+i.content[-1]
              otheroperand = "#O=" + self.content[0].translate(vocParse,types,args[0]  + recursiveArg +["#=",returnType])
              return self.content[1].translate(vocParse,types,args[0] + [otheroperand])
             else:
              return self.content[0].translate(vocParse,types,args[0] + ["#="]) + " is equal to " + self.content[1].translate(vocParse,types,args[0] + recursiveArg + ["#="])
            else:
             newargs = list(args[0])
             newargs.remove("not")
             if not newargs:
              newargs = []
             if self.content[0].kind == "vocsymbol" and len(self.content[0].content) !=0:
              #determine the return type of the function
              returnType = ""
              for i in vocParse.content:
               if i.kind=="function" and i.label==self.content[0].label:
                returnType = "#T="+i.content[-1]
              otheroperand = "#O=" + self.content[1].translate(vocParse,types,newargs + recursiveArg + ["#=",returnType])
              return self.content[0].translate(vocParse,types,args[0] + [otheroperand])
             elif self.content[1].kind == "vocsymbol" and len(self.content[1].content) !=0:
              #determine the return type of the function
              returnType = ""
              for i in vocParse.content:
               if i.kind=="function" and i.label==self.content[1].label:
                returnType = "#T="+i.content[-1]
              otheroperand = "#O=" + self.content[0].translate(vocParse,types,newargs  + recursiveArg + ["#=",returnType])
              return self.content[1].translate(vocParse,types,args[0] + [otheroperand])
             else:
              return self.content[0].translate(vocParse,types,newargs+["#="]) + " is different from " + self.content[1].translate(vocParse,types,newargs + recursiveArg + ["#="])
        elif self.label == '~=':
            # if the left or right operand of the '~=' expression is a vocabulary function,
            # translate it using its negative descriptive translation and translate the other operand using its objective translation
            # in a negation context, translate one vocabulary function using its regular descriptive translation
            #
            # if both operands are compound (arithmetic) expression, translate the '~=' operator using 'is different from'
            # if the '~=' operator is in a negation context, their is a double negation, so we translate using '='
            
            # first check whether a symbol occurs inside a 'self refence' scope.
            # if one operand is a function/predicate (vocsymbol with content) and the other is a constant or a variable (vocsymbol without content)
            # check if that constant/variable already occured as an argument in the functionpredicate
            # if both operands are vocsymbols without content and are the same, the second occurence is treated as a self-reference
            # the self-reference check only searches for reference up to one nested level
            recursiveArg = []
            if self.content[0].kind == "vocsymbol" and len(self.content[0].content) != 0:
             if self.content[1].kind == "vocsymbol" and len(self.content[1].content) == 0:
              if self.content[1].label in [i.label for i in self.content[0].content]:
               recursiveArg.append("#R="+self.content[1].label)
              elif self.content[1].kind == "vocsymbol" and len(self.content[1].content) != 0:
               for i in self.content[0].content:
                if i.label in [j.label for j in self.content[1].content]:
                 recursiveArg.append("#R="+i.label)
            elif self.content[1].kind == "vocsymbol" and len(self.content[1].content) != 0:
             if self.content[0].kind == "vocsymbol" and len(self.content[0].content) == 0:
              if self.content[0].label in [i.label for i in self.content[1].content]:
               recursiveArg.append("#R="+self.content[1].label)
              elif self.content[0].kind == "vocsymbol" and len(self.content[0].content) != 0:
               for i in self.content[1].content:
                if i.label in [j.label for j in self.content[0].content]:
                 recursiveArg.append("#R="+i.label)
            elif self.content[0].kind == "vocsymbol" and self.content[1].kind == "vocsymbol" and len(self.content[0].content) == 0 and len(self.content[1].content) == 0:
              if self.content[0].label == self.content[1].label:
               recursiveArg.append("#R="+ self.content[0].label)
            
            if "not" not in args[0]:
             if self.content[0].kind == "vocsymbol" and len(self.content[0].content) !=0:
              #determine the return type of the function
              returnType = ""
              for i in vocParse.content:
               if i.kind=="function" and i.label==self.content[0].label:
                returnType = "#T="+i.content[-1]
              otheroperand = "#O=" + self.content[1].translate(vocParse,types,args[0] + recursiveArg + ["#=",returnType])
              return self.content[0].translate(vocParse,types,args[0] + ["not", otheroperand])
             elif self.content[1].kind == "vocsymbol" and len(self.content[1].content) !=0:
              #determine the return type of the function
              returnType = ""
              for i in vocParse.content:
               if i.kind=="function" and i.label==self.content[1].label:
                returnType = "#T="+i.content[-1]
              otheroperand = "#O=" + self.content[0].translate(vocParse,types,args[0] + recursiveArg + ["#=",returnType])
              return self.content[1].translate(vocParse,types,args[0] + ["not", otheroperand])
             else:
              return self.content[0].translate(vocParse,types,args[0] + ["#="]) + " is different from " + self.content[1].translate(vocParse,types,args[0] + recursiveArg  + ["#="])
            else:
             newargs = list(args[0])
             newargs.remove("not")
             if not newargs:
              newargs = []
             if self.content[0].kind == "vocsymbol" and len(self.content[0].content) !=0:
              #determine the return type of the function
              returnType = ""
              for i in vocParse.content:
               if i.kind=="function" and i.label==self.content[0].label:
                returnType = "#T="+i.content[-1]
              otheroperand = "#O=" + self.content[1].translate(vocParse,types,newargs + recursiveArg + ["#=",returnType])
              return self.content[0].translate(vocParse,types,newargs + [otheroperand])
             elif self.content[1].kind == "vocsymbol" and len(self.content[1].content) !=0:
              #determine the return type of the function
              returnType = ""
              for i in vocParse.content:
               if i.kind=="function" and i.label==self.content[1].label:
                returnType = "#T="+i.content[-1]
              otheroperand = "#0=" + self.content[0].translate(vocParse,types,newargs  + recursiveArg + ["#=",returnType])
              return self.content[1].translate(vocParse,types,newargs + [otheroperand])
             else:
              return self.content[0].translate(vocParse,types,newargs+["#="]) + " is equal to " + self.content[1].translate(vocParse,types,newargs + recursiveArg + ["#="])
        
        elif self.label == '<':
            if "not" in args[0]:
             newargs = []
             for i in args[0]:
              if i != "not":
               newargs.append(i)
             return self.content[0].translate(vocParse,types,newargs) + " is larger or equal than " + self.content[1].translate(vocParse,types,newargs)
            return self.content[0].translate(vocParse,types,args[0]) + " is smaller than " + self.content[1].translate(vocParse,types,args[0])
        elif self.label == '>':    
            if "not" in args[0]:
             newargs = []
             for i in args[0]:
              if i != "not":
               newargs.append(i)
             return self.content[0].translate(vocParse,types,newargs) + " is smaller or equal than " + self.content[1].translate(vocParse,types,newargs)
            return self.content[0].translate(vocParse,types,args[0]) + " is larger than " + self.content[1].translate(vocParse,types,args[0])
        elif self.label == '>=':
            if "not" in args[0]:
             newargs = []
             for i in args[0]:
              if i != "not":
               newargs.append(i)
             return self.content[0].translate(vocParse,types,newargs) + " is smaller than " + self.content[1].translate(vocParse,types,newargs)
            return self.content[0].translate(vocParse,types,args[0]) + " is larger or equal than " + self.content[1].translate(vocParse,types,args[0])
        elif self.label == '=<':
            if "not" in args[0]:
             newargs = []
             for i in args[0]:
              if i != "not":
               newargs.append(i)
             return self.content[0].translate(vocParse,types,newargs) + " is smaller or equal than " + self.content[1].translate(vocParse,types,newargs)
            return self.content[0].translate(vocParse,types,args[0]) + " is larger than " + self.content[1].translate(vocParse,types,args[0])
        
    
class LogicLeftBinOp(BinOp):
    def translate(self,vocParse,types,*args):
        if self.label == '<=>':
            return self.content[0].translate(vocParse,types,args[0]+ ["#=>P"]) + " if and only if " + self.content[1].translate(vocParse,types,args[0]+ ["#=>P"])
        elif self.label == '=>':
            if self.content[1].label =='=>':
             return "if " + self.content[0].translate(vocParse,types,args[0] + ["#=>P"]) + " and " +self.content[1].translate(vocParse,types,args[0])
            else:
             return "if " + self.content[0].translate(vocParse,types,args[0] + ["#=>P"]) + ", then " +self.content[1].translate(vocParse,types,args[0] + ["#=>C"])           
        elif self.label == '<=':
            return "if " + self.content[1].translate(vocParse,types,args[0] + ["#=>P"]) + ", then " +self.content[0].translate(vocParse,types,args[0] + ["#=>C"])
        elif self.label == '|':
            s = self.content[0].translate(vocParse,types,args[0])
            for i in self.content[1:]:
             s+= " or " +  i.translate(vocParse,types,args[0])
            return s
        elif self.label == '&':
            s = self.content[0].translate(vocParse,types,args[0])
            for i in self.content[1:]:
             s+= " and " + i.translate(vocParse,types,args[0])
            return s
        
class UniOp(object):
    def __init__(self,t):
        self.label = t[0][0]
        self.content = [t[0][1]]
        self.kind = "unaryoperator"
    def ast(self):
        Content = [i.ast() for i in self.content]
        return {'Label':self.label,'Content':Content,'Kind':self.kind}
    def infer(self,vocParse):
        result = OrderedDict()
        [result.update(i.infer(vocParse)) for i in self.content]
        return result
 

class LogicUniOp(UniOp):
    def translate(self,vocParse,types,*args):
        # in case of a single negation over an '=' or '~=' expression, translate that expression in a negative context
        # in case of a single negation over a vocabulary symbol (this should be a predicate or proposition), 
        # use the negative descriptive translation from the vocabulary
        # in case of a negation occuring in a negation (or 'ignore negation') context, the negation context is removed
        if self.label == '~':
         if "ignore-not" not in args[0]:
            if self.content[0].label == '=' or self.content[0].label == '<' or self.content[0].label == '>' or self.content[0].label == '=<' or self.content[0].label == '>=' or self.content[0].label == '~=' or self.content[0].kind == 'vocsymbol':
             return self.content[0].translate(vocParse,types,args[0] + ["not"])
            else:
             return "it is not true that " + self.content[0].translate(vocParse,types,args[0])
         elif "ignore-not" in args[0]:
            newargs = list(args[0])
            newargs.remove("ignore-not")
            if not newargs:
             newargs = []
            return self.content[0].translate(vocParse,types,newargs)
         elif "not" in args[0]:
            newargs = list(args[0])
            newargs.remove("not")
            if not newargs:
             newargs = []
            return self.content[0].translate(vocParse,types,newargs)

class ArithmeticUniOp(UniOp):
    def translate(self,vocParse,types,*args):
        if self.label == '-':
            return "minus " + self.content[0].translate(vocParse,types,args[0])

class ArithmeticLeftBinOp(BinOp):
    def translate(self,vocParse,types,*args):
        if self.label == '/':
            for s in args[0]:
             if s.startswith("#T="):
              for l in vocParse.content:
                if l.kind == "function" and l.label == s[3:]:
                 for m in l.annotation.content:
                  if m.kind == "annotation-nlstringtag" and m.label == "-":
                   #override the translation of the '+' operator
                   translation = ""
                   for word in m.content.content:
                    if word == "#1":
                     translation += self.content[0].translate(vocParse,types,args[0]) + " "
                    elif word == "#2":
                     translation += self.content[1].translate(vocParse,types,args[0]) + " "
                    else:
                     translation += word + " "
                   return translation[:-1]
            return self.content[0].translate(vocParse,types,args[0]) + " divided by " + self.content[1].translate(vocParse,types,args[0])
        
        elif self.label == '*':
            for s in args[0]:
             if s.startswith("#T="):
              for l in vocParse.content:
                if l.kind == "type" and l.label == s[3:]:
                 for m in l.annotation.content:
                  if m.kind == "annotation-nlstringtag" and m.label == "-":
                   #override the translation of the '+' operator
                   translation = ""
                   for word in m.content.content:
                    if word == "#1":
                     translation += self.content[0].translate(vocParse,types,args[0]) + " "
                    elif word == "#2":
                     translation += self.content[1].translate(vocParse,types,args[0]) + " "
                    else:
                     translation += word + " "
                   return translation[:-1]
            return self.content[0].translate(vocParse,types,args[0]) + " times " + self.content[1].translate(vocParse,types,args[0])
        
        elif self.label == '-':
            for s in args[0]:
             if s.startswith("#T="):
              for l in vocParse.content:
                if l.kind == "type" and l.label == s[3:]:
                 for m in l.annotation.content:
                  if m.kind == "annotation-nlstringtag" and m.label == "-":
                   #override the translation of the '+' operator
                   translation = ""
                   for word in m.content.content:
                    if word == "#1":
                     translation += self.content[0].translate(vocParse,types,args[0]) + " "
                    elif word == "#2":
                     translation += self.content[1].translate(vocParse,types,args[0]) + " "
                    else:
                     translation += word + " "
                   return translation[:-1]
            return self.content[0].translate(vocParse,types,args[0]) + " minus " + self.content[1].translate(vocParse,types,args[0])
        elif self.label == '+':
            for s in args[0]:
             if s.startswith("#T="):
              for l in vocParse.content:
                if l.kind == "type" and l.label == s[3:]:
                 for m in l.annotation.content:
                  if m.kind == "annotation-nlstringtag" and m.label == "+":
                   #override the translation of the '+' operator
                   translation = ""
                   for word in m.content.content:
                    if word == "#1":
                     translation += self.content[0].translate(vocParse,types,args[0]) + " "
                    elif word == "#2":
                     translation += self.content[1].translate(vocParse,types,args[0]) + " "
                    else:
                     translation += word + " "
                   return translation[:-1]
            else:
             return self.content[0].translate(vocParse,types,args[0]) + " plus " + self.content[1].translate(vocParse,types,args[0])
            
# BEGIN FO(.) EXPRESSION GRAMMAR

LPAREN = Suppress("(")
RPAREN = Suppress(")")
LBRACK = Suppress("{")
RBRACK = Suppress("}")
LHOOK = Suppress("[")
RHOOK = Suppress("]") 
COLON = Suppress(":")

TRUE = Literal("true")
FALSE = Literal("false")

ARGUMENT = Word(alphas,alphanums+"_")

logicOperand = TRUE | FALSE
logicOperand.setParseAction(LogicOperand)

valueOperand = Word("0123456789")
valueOperand.setParseAction(ValueOperand)

functionOperand = Forward()
arithmeticExpr = Forward()
functionOperand << ((ARGUMENT + Suppress("(")+delimitedList(arithmeticExpr | functionOperand)+Suppress(")"))| ARGUMENT)
functionOperand.setParseAction(FunctionOperand)

aggregateTermExpr = valueOperand | functionOperand

logicFormula = Forward()

quantorArgList = delimitedList(Group(ARGUMENT + Optional(LHOOK+ARGUMENT+RHOOK)),delim=",")

noTermExpr = (Literal("#")|Literal("card")) + LBRACK + quantorArgList + COLON + logicFormula + RBRACK
noTermExpr.setParseAction(NoTermAggregateExpr)

termExpr = (Literal("sum")|Literal("prod")|Literal("max")|Literal("min")) + LBRACK + quantorArgList + COLON + logicFormula + COLON + aggregateTermExpr + RBRACK
termExpr.setParseAction(TermAggregateExpr)

aggregateExpr = noTermExpr | termExpr

arithmeticExprOperand = functionOperand | valueOperand | aggregateExpr
arithmeticExpr << infixNotation( arithmeticExprOperand,
    [
       ("-", 1, opAssoc.RIGHT, ArithmeticUniOp),
       (oneOf("* /"), 2 , opAssoc.LEFT, ArithmeticLeftBinOp),
       (oneOf("+ -"), 2 , opAssoc.LEFT, ArithmeticLeftBinOp),
    ])

# define a comparison expression
comparisonExprOperand = aggregateExpr | arithmeticExpr | functionOperand
comparisonExpr = Group(comparisonExprOperand + oneOf("= ~= < > <= >=") + comparisonExprOperand)
comparisonExpr.setParseAction(ComparisonBinOp)

# define expressions with boolean operators
quantifiedExpr = Forward()
logicExprOperand = logicOperand | quantifiedExpr | comparisonExpr | functionOperand

logicExpr = infixNotation( logicExprOperand,
    [
        ("~", 1, opAssoc.RIGHT, LogicUniOp),
        (oneOf("&"), 2, opAssoc.LEFT,  LogicLeftBinOp),
        (oneOf("|"), 2, opAssoc.LEFT,  LogicLeftBinOp),
        (oneOf("<="), 2, opAssoc.LEFT,  LogicLeftBinOp),
        (oneOf("=>"), 2, opAssoc.LEFT,  LogicLeftBinOp),
        (oneOf("<=>"), 2, opAssoc.LEFT,  LogicLeftBinOp),
    ])

# define quantified expressions
FORALL = Literal("!")
EXISTS = Literal("?")
EXISTS_ONE = Literal("?=1")

quantifiedExpr << (FORALL | EXISTS_ONE | EXISTS) + quantorArgList + Suppress(":") + (quantifiedExpr | logicExpr)
quantifiedExpr.setParseAction(QuantorOperand)

# also accept unquantified expressions
logicFormula << (quantifiedExpr | logicExpr)
formulaDef = logicFormula + Suppress(".")

# define FO(ID) definitions
reducedComparisonExpr = Group(functionOperand + Literal("=") + arithmeticExpr)
reducedComparisonExpr.setParseAction(ArithmeticLeftBinOp)
definitionHead = reducedComparisonExpr | functionOperand
definitionExpr = definitionHead + Optional(Suppress("<-") + logicFormula)
definitionExpr.setParseAction(DefinitionExpr)

extQuantifiedExpr = (FORALL) + quantorArgList + Suppress(":") + definitionExpr
extQuantifiedExpr.setParseAction(QuantorOperand)

extLogicFormula = extQuantifiedExpr | definitionExpr
extFormulaDef = extLogicFormula + Suppress(".")

# END FO(.) EXPRESSION GRAMMAR

class IdpProgram(object):
    def __init__(self,t):
        self.kind = 'program'
        self.content = t
    def ast(self):
        return {'Kind':self.kind,'Content':[i.ast() for i in self.content]}
    def translate(self, programParse, *args):
        somethingthere = False
        vocThere = False
        result = ""
        for i in self.content:
         if i.kind == 'vocabulary':
          vocThere = True
         if i.kind == 'theory' or i.kind == 'structure':
          somethingthere = True
          break
        for i in self.content:
         result += i.translate(programParse,[]) + "\n"
        if not vocThere:
         result += "No vocabularies detected.\n\n"
        if not somethingthere:
         result += "No theory or structure detected to translate.\nEither nothing was defined or a parsing error occured.\n\n"
        return result
    
class IdpSection(object):
    def ast(self):
        return {'Kind':self.kind,'Value':self.label,'Content':[i.ast() for i in self.content]}

class IdpVocSection(IdpSection):
    def __init__(self,t): 
        self.kind = t[0]
        self.label = t[1]
        self.content = t[2:]
    def translate(self, programParse, *args):
        return "A vocabulary with name " + self.label + " was found.\n\n"+"AST: " + str(self.ast()) + "\n\n"

class IdpTheorySection(IdpSection):
    def __init__(self,t):
        self.kind = t[0]
        self.label = t[1:3]
        self.content = t[3]
    def translate(self, programParse, *args):
        vocParse = None
        for i in programParse.content:
         if i.kind == "vocabulary" and i.label == self.label[1]:
          vocParse = i
          break
        if vocParse == None:
         return "Theory " + self.label[0] + " could not be translated since no vocabulary " + self.label[1] + " could be found.\n\n"
        else:
         result = "Translation of theory " + self.label[0] + " over vocabulary " + self.label[1] + ":" + "\n\n"
         for i in self.content:
          result += "AST: " + str(i.ast()) + "\n\n" + "translation: " + i.translate(vocParse,[]) + "\n\n"
         return result
         
class IdpStructureSection(IdpSection):
    def __init__(self,t):
        self.kind = t[0]
        self.label = t[1:3]
        self.content = t[3]
    def translate(self, programParse, *args):
        vocParse = None
        for i in programParse.content:
         if i.kind == "vocabulary" and i.label == self.label[1]:
          vocParse = i
          break
        if vocParse == None:
         return "Structure " + self.label[0] + " could not be translated since no vocabulary " + self.label[1] + " could be found.\n\n"
        else:
         result = "Translation of structure " + self.label[0] + " over vocabulary " + self.label[1] + ":" + "\n\n"
         for i in self.content:
          result += "AST: " + str(i.ast()) + "\n\n" + "translation: " + i.translate(vocParse,[]) + "\n\n"
         return result

class IdpVocExpr(object):
    def __init__(self,t):
        self.label = t[0]
        self.content = t[1:-1]
        self.annotation = t[-1]
    def ast(self):
        return {'Kind':self.kind,'Label':self.label,'Content':self.content,'Annotation':self.annotation.ast()}

class IdpTypeDef(IdpVocExpr):
   kind = 'type'
class IdpPredDef(IdpVocExpr):
   kind = 'predicate'
class IdpFuncDef(IdpVocExpr):
   kind = 'function'

class IdpTheoryExpr(object):
    def __init__(self,t):
        self.content = t[0]
    def ast(self):
        return {'Kind':self.kind,'Content':self.content.ast()}
    def infer(self,vocParse):
        # collect all the variable/constant types recursively in an ordered dictionary
        types = self.content.infer(vocParse)
        used = {}
        # add referring information to the types
        for k,v in types.iteritems():
         if v in used:
          used[v]+=1
          types[k] = [types[k],used[v],False]
         else:
          used[v]=1
          types[k] = [types[k],used[v],False]
        return types    
    def translate(self,vocParse,*args):
        # run type inference
        types = self.infer(vocParse)
        # translate the formula
        return (lambda x: x[0].upper() + x[1:])(self.content.translate(vocParse,types,[]) + ".")

class IdpFormulaDef(IdpTheoryExpr):
    kind = 'formula'

class IdpGroupedTheoryExpr(object):
    def __init__(self,t):
        self.content = t[0:]
        self.kind='definition'
    def ast(self):
        return {'Kind':self.kind,'Content': [i.ast() for i in self.content]}
    def translate(self,vocParse,*args):
        translation = "The following definition holds:\n"
        for el in self.content:
         # translate every formula in the group
         translation+=el.translate(vocParse,[])+"\n"
        return translation
      
class IdpStructureExpr(object):
    def __init__(self,t):
        self.label = t[0]
        self.content = t[1:]
    def ast(self):
        return {'Kind':self.kind,'Label':self.label,'Content':self.content}
    def infer(self,vocParse):
         # search the vocParse for a function/predicate that matches the label
         for i in vocParse.content:
          if (i.kind == "predicate" or i.kind == "function") and i.label==self.label and len(self.content[0])==len(i.content): 
           return {i.label:i.content}
        
class IdpScopeDef(IdpStructureExpr):
    kind = 'scope-def'
    def translate(self,vocParse,*args):
     # translate the scope declaration to an 'and' sentence.
     translation = ""
     for i in vocParse.content:
       if (i.kind == "type" and i.label==self.label):
        #translate the discrete description of the type's scope
        for j in i.annotation.content:
         if j.kind == "annotation-nlstringtag" and j.label == "description":
          # use the prescribed translation
          for pos in range(0,len(self.content)):
           temp = list(j.content.content)
           for k in range(0,len(temp)):
            if temp[k] == "#" + str(i):
             temp[k]=self.content[pos][0]
          # combine the translated fragments
           for el in temp[0:-1]:
            translation += el + " "
           translation += self.content[-1][0]
           if pos != len(self.content)-1:
            # put an and between definitions
            translation += " and "
        else:
         # default translation "x is a T"
         for pos in range(0,len(self.content)):
          translation += self.content[pos][0] + " is a " + self.label
          if pos != len(self.content)-1:
           translation += " and "
       
       elif i.kind == "predicate" and i.label==self.label and len(self.content[0])==0:
         # handle the 'proposition is false' case
         for j in i.annotation.content:
          if j.kind == "annotation-nlstringtag" and j.label == "~description":
           temp = j.content.content
           for el in temp[0:-1]:
            translation += el + " "
           translation += temp[-1]
           break
         if translation=="":
          for j in i.annotation.content:
           if j.kind == "annotation-nlstringtag" and j.label == "description":
            temp = j.content.content
            translation = "It is false that "
            for el in temp[0:-1]:
             translation += el + " "
       elif i.kind == "predicate" and i.label==self.label and len(self.content[0])==1 and self.content[0]=="()":
          # handle the 'proposition is true' case
         for j in i.annotation.content:
          if j.kind == "annotation-nlstringtag" and j.label == "description":
           temp = j.content.content
           for el in temp[0:-1]:
            translation += el + " "
           translation += temp[-1]
           break  
       elif (i.kind == "predicate" or i.kind == "function") and i.label==self.label and len(self.content[0])==len(i.content):
        #collect the types of the arguments
        types = [t for t in i.content]
        for j in i.annotation.content:
         if j.kind == "annotation-nlstringtag" and j.label == "description":
          # translate the predicate/function as a description, for each entry in the scoping list
          for pos in range(0,len(self.content)):
           temp = list(j.content.content)
           i = 1
           for k in range(0,len(temp)):
            if temp[k] == "#" + str(i):
             temp[k]=None
             # check if there is translation defined for this type
             for l in vocParse.content:
              if l.kind=="type" and l.label==types[i-1]:
               for m in l.annotation.content:
                if m.kind == "annotation-nlstringtag" and m.label == "description":
                 tr = m.content.content
                 temp[k]=""
                 for n in range(0,len(tr)):
                  if tr[n]=="#":
                   temp[k] += self.content[pos][i-1] + " " 
                  else:
                   temp[k] += tr[n] + " "
                 temp[k] = temp[k][:-1]
                 break
               break
             if not temp[k]:
              # use the direct interpretation as translation
              temp[k] = self.content[pos][i-1]
             i+=1
            elif temp[k] == "#R":
             temp[k]=None
             # check if there is translation defined for this type
             for l in vocParse.content:
              if l.kind=="type" and l.label==types[-1]:
               for m in l.annotation.content:
                if m.kind == "annotation-nlstringtag" and m.label == "description":
                 tr = m.content.content
                 temp[k]=""
                 for n in range(0,len(tr)):
                  if tr[n]=="#":
                   temp[k] += self.content[pos][-1] + " " 
                  else:
                   temp[k] += tr[n] + " "
                 #strip the last space
                 temp[k] = temp[k][:-1]
                 break
               break
             if not temp[k]:
              # use the direct interpretation as translation
              temp[k] = self.content[pos][-1]

           for el in temp[0:-1]:
            translation += el + " "
           translation += temp[-1]
           if pos != len(self.content)-1:
            # put an and between definitions
            translation += " and "      
     # capitalize, punctuate and return the sentence
     return translation[0].upper() + translation[1:] + "."

class IdpRangeDef(IdpStructureExpr):
    kind = 'range-def'
    def translate(self,vocParse,*args):
     # translate the range declaration to an 'takes a value from' sentence
     translation = "'" + self.label + "' takes a value from " + self.content[0]+ " to " + str(self.content[1])
     # capitalize and punctuate the sentence
     return translation[0] + translation[1].upper() + translation[2:] + "."

class IdpInterpretationDef(IdpStructureExpr):
    kind = 'interpretation-def'
    def translate(self,vocParse,*args):
     # translate the range declaration to an 'it is certainly false/true/unknown that' sentence
     interpretation = {"ct":"certainly true", "cf": "certainly false", "u":"undefined"}
     
     bodytranslation = ""
     for i in vocParse.content:
       if (i.kind == "function" or i.kind == "predicate") and i.label==self.label and len(self.content[1])==len(i.content):
        for j in i.annotation.content:
         if j.kind == "annotation-nlstringtag" and j.label == "description":
          # translate the predicate/function as a description, for each entry in the scoping list
          for pos in range(1,len(self.content)):
           temp = list(j.content.content)
           i = 1
           for k in range(0,len(temp)):
            if temp[k] == "#" + str(i):
             temp[k] = self.content[pos][i-1]
             i+=1
            elif temp[k] == "#R":
             for element in args:
              temp[k] = self.content[pos][-1]
           for el in temp[0:-1]:
            bodytranslation += el + " "
           bodytranslation += temp[-1]
           if pos != len(self.content)-1:
            # put an and between definitions
            bodytranslation += " and "            
     translation = "it is " + interpretation[self.content[0]] + " that " + bodytranslation
     # capitalize and punctuate the sentence
     return translation[0].upper() + translation[1:] + "."
 
class Annotation(object):
    def __init__(self,t):
        self.content = t
    def ast(self):
        return {'Kind':self.kind, 'Content':[i.ast() for i in self.content]}

class TypeAnnotation(Annotation):    
    kind = 'type-annotation'
class PredAnnotation(Annotation):    
    kind = 'predicate-annotation'
class FuncAnnotation(Annotation):    
    kind = 'function-annotation' 

class LabelAnnotationTag(object):
   def __init__(self,t):
        self.kind = 'annotation-labeltag'
        self.label = t[0]
   def ast(self):
       return {'Kind':self.kind, 'Label':self.label}

class NLStringAnnotationTag(object):
   def __init__(self,t):
       self.kind = 'annotation-nlstringtag'
       self.label = t[0]
       self.content = t[1]
   def ast(self):
       return {'Kind':self.kind, 'Label':self.label, 'Content':self.content.ast()}

class NLString(object):
   def __init__(self,t):
       self.kind = 'nlstring'
       self.content = t
   def ast(self):
       return {'Kind':self.kind, 'Content':self.content}

# BEGIN IDP PROGRAM GRAMMAR

nlArg = Word("#","01234567489R")
nlWord = Word(alphas+",'") #+ Suppress("/") + nlKeyword
nlString = Suppress("\"") + ZeroOrMore(nlWord|nlArg) + Suppress("\"")
nlString.setParseAction(NLString)

typeTag = Keyword("ENTITY") | Keyword("THING")
descriptionTag = Keyword("description") + nlString
negDescriptionTag = Keyword("~description") + nlString
equalityTag = Keyword("=") + nlString
operatorTag = (Keyword("+")|Keyword("-")|Keyword("*")|Keyword("/")) + nlString
typeTag.setParseAction(LabelAnnotationTag)
descriptionTag.setParseAction(NLStringAnnotationTag)
negDescriptionTag.setParseAction(NLStringAnnotationTag)
equalityTag.setParseAction(NLStringAnnotationTag)
operatorTag.setParseAction(NLStringAnnotationTag)
combinedDescriptionTag = (descriptionTag + negDescriptionTag) | descriptionTag
typeAnnotation = (Suppress("#") + Suppress("TYPE") + typeTag + descriptionTag + OneOrMore(operatorTag))|(Suppress("#") + Suppress("TYPE") + typeTag + descriptionTag)|(Suppress("#") + Suppress("TYPE") + typeTag)
predAnnotation = Suppress("#") + Suppress("PREDICATE") + combinedDescriptionTag
funcAnnotation = Suppress("#") + Suppress("FUNCTION") + combinedDescriptionTag + equalityTag
typeAnnotation.setParseAction(TypeAnnotation)
predAnnotation.setParseAction(PredAnnotation) 
funcAnnotation.setParseAction(FuncAnnotation)

typeDef = Suppress("type") + ARGUMENT + Optional((Suppress(Keyword("isa")) + ARGUMENT) | (Suppress(Keyword("constructed from")) + LBRACK + Group(delimitedList(ARGUMENT)) + RBRACK)) + typeAnnotation + Suppress(lineEnd)
numericTypeDef = Suppress("type") + ARGUMENT + Suppress(Keyword("=")) + LBRACK + delimitedList(Word("-0123456789"), delim=";") + RBRACK + Suppress(Keyword("isa")) + ARGUMENT + typeAnnotation + Suppress(lineEnd)
funcDef = Optional(Suppress("partial")) + ARGUMENT + Optional(LPAREN + delimitedList(ARGUMENT) + RPAREN) + COLON + ARGUMENT + funcAnnotation + (Suppress(lineEnd))
predDef = ARGUMENT + Optional(LPAREN + delimitedList(ARGUMENT) + RPAREN) + predAnnotation + (Suppress(lineEnd))

typeDef.setParseAction(IdpTypeDef)
numericTypeDef.setParseAction(IdpTypeDef)
predDef.setParseAction(IdpPredDef)
funcDef.setParseAction(IdpFuncDef)
idpVocExpr = numericTypeDef | typeDef | predDef | funcDef
idpVocabularyBody= ZeroOrMore(idpVocExpr)
idpVocabularyBody.ignore("//" + restOfLine)
#idpVocabularyBody.setDefaultWhitespaceChars(" \t")
idpVocabulary = Keyword("vocabulary") + ARGUMENT + LBRACK + idpVocabularyBody + RBRACK
#idpVocabulary.setDefaultWhitespaceChars(" \n\t")
idpVocabulary.setParseAction(IdpVocSection)

formulaDef.setParseAction(IdpFormulaDef)
extFormulaDef.setParseAction(IdpFormulaDef)
idpTheoryExpr = formulaDef
idpGroupedTheoryExpr = LBRACK + ZeroOrMore(extFormulaDef) + RBRACK
idpGroupedTheoryExpr.setParseAction(IdpGroupedTheoryExpr)
idpTheoryBody = Group(ZeroOrMore(idpTheoryExpr|idpGroupedTheoryExpr))
idpTheory = Keyword("theory") + ARGUMENT + COLON + ARGUMENT + LBRACK + idpTheoryBody + RBRACK
#idpTheory.setDefaultWhitespaceChars(" \n\t")
idpTheory.setParseAction(IdpTheorySection)

idpPredScopeDef = ARGUMENT + Suppress("=") + LBRACK + delimitedList(Group(LPAREN+delimitedList(Word(alphanums))+RPAREN)|Group(delimitedList(Word(alphanums))),delim=";") + RBRACK
idpPropScopeDef =  ARGUMENT + Suppress("=") + LBRACK + Optional("()") + RBRACK
idpFuncScopeDef = ARGUMENT + Suppress("=") + LBRACK + delimitedList(Group(Optional(delimitedList(Word(alphanums))) + Suppress("->") + Word(alphanums)),delim=";") + RBRACK
idpConsScopeDef = ARGUMENT + Suppress("=") + Group(Word(alphanums))
idpRangeDef = ARGUMENT + Suppress("=") + LBRACK + Word(alphanums) + Suppress("..") + Word(alphanums)+ RBRACK
idpInterpretationDef = ARGUMENT + Suppress("<") + oneOf("ct cf u") + Suppress(">") + Suppress("=") + LBRACK + delimitedList(Group(Optional(delimitedList(ARGUMENT)) + Suppress("->") + ARGUMENT)|Group(LPAREN+ delimitedList(ARGUMENT)+RPAREN),delim=";") + RBRACK
idpPredScopeDef.setParseAction(IdpScopeDef)
idpPropScopeDef.setParseAction(IdpScopeDef)
idpFuncScopeDef.setParseAction(IdpScopeDef)
idpConsScopeDef.setParseAction(IdpScopeDef) 
idpRangeDef.setParseAction(IdpRangeDef)
idpInterpretationDef.setParseAction(IdpInterpretationDef)
idpStructureExpr = idpPredScopeDef|idpPropScopeDef|idpFuncScopeDef|idpConsScopeDef|idpRangeDef|idpInterpretationDef
idpStructureBody = Group(ZeroOrMore(idpStructureExpr))
idpStructure = (Keyword("structure")|Keyword("factlist")) + ARGUMENT + COLON + ARGUMENT + LBRACK + idpStructureBody + RBRACK
#idpStructure.setDefaultWhitespaceChars(" \n\t")
idpStructure.setParseAction(IdpStructureSection)

ignoredSection = Suppress((Keyword("term")|Keyword("query")|Keyword("procedure")) + SkipTo("}",include=True))
program = ZeroOrMore(idpVocabulary|idpTheory|idpStructure|ignoredSection)
program.ignore("//" + restOfLine + lineEnd)
program.setParseAction(IdpProgram)

# END IDP PROGRAM GRAMMAR

# BEGIN MAIN PROGRAM
   
if __name__ == "__main__":

    parser = argparse.ArgumentParser(description='This program generates a NL English translation from an IDP model.')
    parser.add_argument('--model', type=argparse.FileType('r'), required=True,
                    help='the name of the text file containing the IDP model.')

    args = parser.parse_args()
     
    try:
     t1 = timer()
     print "program started\n"
     model=args.model.read()
     print "IDP specification:\n"
     print model,"\n\n"
     res = program.parseString(model)
     t2 = timer()
     print "translation:\n"
     print res[0].translate(res[0],[]) if res else ""
    except ParseException as pe:
            print "could not parse idp program:",pe
            print "error at position",pe.col
    print "program completed\n"
    t3 = timer()
    print "parsing time: " + str(t2-t1) + " seconds"
    print "translation time: " + str(t3-t2)+ " seconds"

# END MAIN PROGRAM

# END OF PROGRAM