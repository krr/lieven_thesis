program started

translation:

A vocabulary with name V was found.

AST: {'Content': [{'Content': [], 'Kind': 'type', 'Annotation': {'Content': [{'Kind': 'annotation-labeltag', 'Label': 'THING'}], ... , 'Kind': 'vocabulary', 'Value': 'V'}


Translation of theory T over vocabulary V:

AST: {'Content': {'Content': [{'Kind': 'vocsymbol', 'Label': 'Start'}, {'Kind': 'vocsymbol', 'Label': 'End'}], 'Kind': 'vocsymbol', 'Label': 'Reaches'}, 'Kind': 'formula'}

translation: The start is reachable from the end.

AST: {'Content': {'Content': {'Content': [{'Content': [{'Content': [{'Kind': 'vocsymbol', 'Label': 'x'}, {'Kind': 'vocsymbol', 'Label': 'y'}], 'Kind': 'vocsymbol', 'Label': 'Path'}, {'Kind': 'vocsymbol', 'Label': 'd'}], 'Kind': 'binaryoperator', 'Label': '='}, {'Content': [{'Content': [{'Kind': 'vocsymbol', 'Label': 'x'}, {'Kind': 'vocsymbol', 'Label': 'y'}], 'Kind': 'vocsymbol', 'Label': 'Road'}, {'Kind': 'vocsymbol', 'Label': 'd'}], 'Kind': 'binaryoperator', 'Label': '='}], 'Kind': 'binaryoperator', 'Label': '=>'}, 'Kind': 'quantifier', 'Arguments': [(['x'], {}), (['y'], {}), (['d'], {})], 'Label': '!'}, 'Kind': 'formula'}

translation: If a first city has a path to a second city with a distance, then that first city has a road to that second city with that distance.

AST: {'Content': [{'Content': {'Content': {'Content': [{'Content': [{'Kind': 'vocsymbol', 'Label': 'x'}, ... ,'Kind': 'binaryoperator', 'Label': '&'}, 'Kind': 'quantifier', 'Arguments': [(['z'], {})], 'Label': '?'}], 'Kind': 'definition-rule', 'Label': '<-'}, 'Kind': 'quantifier', 'Arguments': [(['x'], {}), (['y'], {})], 'Label': '!'}, 'Kind': 'formula'}], 'Kind': 'definition'}

translation: The following definition holds:
A first city is reachable from a second city when that first city has a path to that second city with some distance or that second city has a path to that first city with that distance.
A first city is reachable from a second city when that first city is reachable from some third city and that third city is reachable from that second city.


Translation of structure S over vocabulary V:

AST: {'Content': [(['Reno'], {}), (['Chicago'], {}), (['Fargo'], {}), (['Minnesota'], {}), (['Buffalo'], {}), (['Toronto'], {}), (['Winslow'], {}), (['Sarasota'], {}), (['Wichita'], {}), (['Tulsa'], {}), (['Ottawa'], {}), (['Oklahoma'], {}), (['Tampa'], {}), (['Panama'], {}), (['Mattawa'], {})], 'Kind': 'scope-def', 'Label': 'city'}

translation: Reno is a city and Chicago is a city and Fargo is a city and Minnesota is a city and Buffalo is a city and Toronto is a city and Winslow is a city and Sarasota is a city and Wichita is a city and Tulsa is a city and Ottawa is a city and Oklahoma is a city and Tampa is a city and Panama is a city and Mattawa is a city.

AST: {'Content': [(['Reno', 'Chicago', '9'], {}), (['Fargo', 'Buffalo', '3'], {}), (['Chicago', 'Minnesota', '7'], {}), (['Minnesota', 'Toronto', '2'], {}), (['Toronto', 'Buffalo', '4'], {}), (['Winslow', 'Sarasota', '12'], {}), (['Winslow', 'Buffalo', '11'], {}), (['Tampa', 'Oklahoma', '2'], {}), (['Panama', 'Mattawa', '1'], {}), (['Panama', 'Tampa', '11'], {}), (['Oklahoma', 'Tulsa', '17'], {}), (['Ottawa', 'Minnesota', '3'], {}), (['Tulsa', 'Buffalo', '4'], {}), (['Buffalo', 'Tulsa', '10'], {})], 'Kind': 'scope-def', 'Label': 'Road'}

translation: Reno has a road to Chicago with a distance of 9 and Fargo has a road to Buffalo with a distance of 3 and Chicago has a road to Minnesota with a distance of 7 and Minnesota has a road to Toronto with a distance of 2 and Toronto has a road to Buffalo with a distance of 4 and Winslow has a road to Sarasota with a distance of 12 and Winslow has a road to Buffalo with a distance of 11 and Tampa has a road to Oklahoma with a distance of 2 and Panama has a road to Mattawa with a distance of 1 and Panama has a road to Tampa with a distance of 11 and Oklahoma has a road to Tulsa with a distance of 17 and Ottawa has a road to Minnesota with a distance of 3 and Tulsa has a road to Buffalo with a distance of 4 and Buffalo has a road to Tulsa with a distance of 10.

AST: {'Content': [(['Reno'], {})], 'Kind': 'scope-def', 'Label': 'Start'}

translation: The start is Reno.

AST: {'Content': [(['Mattawa'], {})], 'Kind': 'scope-def', 'Label': 'End'}

translation: The end is Mattawa.


program completed

parsing time: 0.331547880432 seconds
translation time: 0.00273708192858 seconds