
//the einstein puzzle
vocabulary V{
        type House constructed from {Red,Green,Blue,Yellow,White} # TYPE THING description"the # house"
        type Owner constructed from {English, Swede, Dane, Norwegian,German} # TYPE ENTITY description"the #" //comment
        type Beverage constructed from {Tea,Coffee,Milk,Beer,Water} #TYPE THING
        type Pet constructed from {Dog,Bird,Cat,Horse,Fish} # TYPE ENTITY description"the #"
        type Cigar constructed from {PallMall,Dunhill,Blend,Bluemaster,Prince} # TYPE THING
        type Position isa int # TYPE THING description"position #" +"#1 to the left of #2" -"#1 to the right of #2"
        IsOwner(Owner) #PREDICATE description"#1 is an owner" ~description"#1 is not an owner"
        OwnsHouse(Owner):House #FUNCTION description"#R is owned by #1" ~description"#1 does not own #R" ="the house owned by #1"
        OwnsPet(Owner):Pet #FUNCTION description"#1 owns #R" ~description"#1 does not own #R" ="the pet owned by #1"
        Drinks(Owner):Beverage #FUNCTION description"#1 drinks #R" ~description"#1 does not drink #R" ="the drink of #1"
        Smokes(Owner):Cigar #FUNCTION description"#1 smokes #R" ~description"#1 does not smoke #R" ="the cigars smoke by #1"
        Order(House):Position #FUNCTION description"#1 lies at #R" ~description"#1 does not lie at #R" ="the position of #1"
}

theory T:V{
        ! h: ?=1 o:OwnsHouse(o)=h.
        ! p: ?=1 o:OwnsPet(o)=p.
        ! b: ?=1 o:Drinks(o)=b.
        ! n: ?=1 h:Order(h)=n.
        ! c: ?=1 o:Smokes(o)=c.
        
        // The green house is on the left and next to the white house. 
        Order(White)=Order(Green)+1.
        
        // The green homeowner drinks coffee.
        !x:OwnsHouse(x)=Green => Drinks(x)=Coffee.
        
        // The person who smokes Pall Mall rears birds.
        !x:Smokes(x)=PallMall => OwnsPet(x)=Bird.
        
        // The owner of the yellow house smokes Dunhill.
        !x:OwnsHouse(x)=Yellow => Smokes(x)=Dunhill.
        
        // The man living in the center house drinks milk.
        !h,x:OwnsHouse(x)=h & Order(h)=3 => Drinks(x)=Milk.
        
        // The Norwegian lives in the first house.
        !h: OwnsHouse(Norwegian)=h => Order(h)=1.
        
        // The man who smokes Blends lives next to the one who keeps cats.
        !x1,x2,h1,h2: OwnsHouse(x1)=h1 & OwnsHouse(x2)=h2 & Smokes(x1)=Blend &OwnsPet(x2)=Cat => (Order(h1)=1+Order(h2) | 1+Order(h1)=Order(h2)).
        
        // The man who keeps the horse lives next to the man who smokes Dunhill.
        !x1,x2,h1,h2: OwnsHouse(x1)=h1 & OwnsHouse(x2)=h2 & OwnsPet(x1)=Horse &Smokes(x2)=Dunhill => (Order(h1)=Order(h2)+1 | 1+Order(h1)=Order(h2)).
        
        // The owner who smokes Bluemaster drinks beer.
        !x : Smokes(x)=Bluemaster => Drinks(x)=Beer.
        
        // The German smokes Prince.
        Smokes(German)=Prince.
        
        // The Norwegian lives next to the blue house.
        !h: OwnsHouse(Norwegian)=h => (Order(h)=Order(Blue)+1 | Order(h)=Order(Blue)-1).
        
        // The man who smokes Blends has a neighbor who drinks water.
        !x1,h1 : OwnsHouse(x1)=h1 & Smokes(x1)=Blend => ?x2,h2:( OwnsHouse(x2)=h2 &(Order(h1)=Order(h2)+1 | Order(h1)=Order(h2)-1) & Drinks(x2)=Water). 
}

structure S:V{
        Nb={1..5}
        
        // Hints
        OwnsHouse<ct> = {English->Red}// The Englishman lives in the red house.
        OwnsPet<ct> = {Swede->Dog} // The Swede keeps dogs as pets.
        Drinks<ct> = {Dane->Tea}  // The Dane drinks tea.
}