# MASTER THESIS OF LIEVEN PAULISSEN #

This is the repository for the master thesis of Lieven Paulissen.

# ABOUT THE THESIS

In this thesis, a NLG system is designed to translate FO(.) specifications into natural English descriptions.

# RELATED PERSONA

* promotor: Pr. Dr. Gerda Janssens en Pr. Dr. Marc Denecker
* daily advisor: Dr. Ir. Jo Devriendt

# REPOSITORY STRUCTURE
* IEEEtemplate: the template used for the scientific article
* final text: contains the final text of the thesis (in Dutch) as well as the scientific paper (in English)
* intermediate text: contains the first intermediate text of the thesis
* kultex: contains the facultairy thesis template as well as latex libraries
* literaturestudy: contains the relevant literature that was consulted for this thesis
* poster: contains the poster that was designed for this thesis
* presentation 1: contains the first intermediate thesis presentation
* presentation 2: contains the second intermediate thesis presentation
* presentation 3: contains the third intermediate thesis presentation
* project: contains the python code for the translater software, as well as translation demo's and design drafts
* report 1: contains the first intermediate report
* report 2: contains the first intermediate report
* slides: contains course and seminar slides relevant to the DTAI thesis program

# PROJECT STATE

You can read the commentary in 'translate.py' to get an idea of the current capabilities of the translation program. The main focus lies on translating the expressions inside the theories and structures of an FO(.) knowledge specification.

# COMPILING THE THESIS LATEX CODE

Follow the install instructions in the 'README.md' of the kultex folder.

Then, you can just open 'masterthesis.tex' in your favorite latex editor.

# HOW TO RUN

The translation program is written in Python 2.7.

In the 'project' directory, place some idp vocabulary, theory and structures in 'model.txt'.

Then install the pyparsing library using:

```
#!python
pip install pyparsing

```
Test the translation program using:

```
#!python
python translation.py --model 'model.txt'

```
You can view the progam help using:

```
#!python
python translation.py -h

```